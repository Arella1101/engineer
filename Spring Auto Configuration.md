#Spring Auto Configuration

## @EnableAutoConfiguration

**@EnableAutoConfiguration**是读*spring-boot-autoconfigure-*.jar*下*/META-INF/spring-autoconfigure-metadata.properties*

如下：

```properties
org.springframework.boot.autoconfigure.couchbase.CouchbaseAutoConfiguration.ConditionalOnClass=com.couchbase.client.java.CouchbaseBucket,com.couchbase.client.java.Cluster

org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration.ConditionalOnClass=org.springframework.amqp.rabbit.core.RabbitTemplate,com.rabbitmq.client.Channel

org.springframework.boot.autoconfigure.jersey.JerseyAutoConfiguration.AutoConfigureOrder=-2147483648

org.springframework.boot.autoconfigure.webservices.WebServicesAutoConfiguration.AutoConfigureAfter=org.springframework.boot.autoconfigure.web.servlet.ServletWebServerFactoryAutoConfiguration

org.springframework.boot.autoconfigure.data.couchbase.CouchbaseReactiveDataAutoConfiguration=
......

```

接下来扫描每个类并根据disable条件删掉某些*configuration*，并去重

## @Conditional

读取每个auto configuration后，根据条件决定是否加载，条件如下：

### @ConditionalOnClass

This configuration is enabled only when these classes are available in the classpath

### @ConditionalOnBean

This configuration is enabled that only matches when the specified bean classes and/or names are already contained in the BeanFactory

### @ConditionalOnMissingBean

that only matches when the specified bean classes and/or names are not already contained in the BeanFactory

### 举例

使用tomcat data source configuration类

***package org.springframework.boot.autoconfigure.jdbc***

```java
@ConditionalOnClass(org.apache.tomcat.jdbc.pool.DataSource.class)
	@ConditionalOnProperty(name = "spring.datasource.type", havingValue = "org.apache.tomcat.jdbc.pool.DataSource", matchIfMissing = true)
	static class Tomcat extends DataSourceConfiguration {

		@Bean
		@ConfigurationProperties(prefix = "spring.datasource.tomcat")
		public org.apache.tomcat.jdbc.pool.DataSource dataSource(
				DataSourceProperties properties) {
			org.apache.tomcat.jdbc.pool.DataSource dataSource = createDataSource(
					properties, org.apache.tomcat.jdbc.pool.DataSource.class);
			DatabaseDriver databaseDriver = DatabaseDriver
					.fromJdbcUrl(properties.determineUrl());
			String validationQuery = databaseDriver.getValidationQuery();
			if (validationQuery != null) {
				dataSource.setTestOnBorrow(true);
				dataSource.setValidationQuery(validationQuery);
			}
			return dataSource;
		}
	}
```

结果就是生成data source的bean供使用

## Debugging Auto Configuration

### Turning on debug logging

1. spring-boot:run运行的在对话框Enviroment中加入debug=true变量 

2. java -jar xx.jar --debug

3. main方法运行，在VM Argumanets加入-Ddebug

4. 直接在application文件中加入debug=true

   ### 输出

   ```
   =========================
   AUTO-CONFIGURATION REPORT
   =========================

   Positive matches:
   -----------------
   DispatcherServletAutoConfiguration matched
    - @ConditionalOnClass classes found: org.springframework.web.servlet.DispatcherServlet (OnClassCondition)
    - found web application StandardServletEnvironment (OnWebApplicationCondition)


   Negative matches:
   -----------------
   ActiveMQAutoConfiguration did not match
    - required @ConditionalOnClass classes not found: javax.jms.ConnectionFactory,org.apache.activemq.ActiveMQConnectionFactory (OnClassCondition)

   AopAutoConfiguration.CglibAutoProxyConfiguration did not match
    - @ConditionalOnProperty missing required properties spring.aop.proxy-target-class (OnPropertyCondition)
   ```

### Spring Boot Actuator

Add ***Actuator***

```
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-actuator</artifactId>
</dependency>

<dependency>
	<groupId>org.springframework.data</groupId>
	<artifactId>spring-data-rest-hal-browser</artifactId>
</dependency>
```

URL: <http://localhost:8080/actuator/#http://localhost:8080/autoconfig>

输出

```xml
{
    "positiveMatches": {
     "DevToolsDataSourceAutoConfiguration": {
            "notMatched": [
                {
                    "condition": "DevToolsDataSourceAutoConfiguration.DevToolsDataSourceCondition", 
                    "message": "DevTools DataSource Condition did not find a single DataSource bean"
                }
            ], 
            "matched": [ ]
        }, 
        "RemoteDevToolsAutoConfiguration": {
            "notMatched": [
                {
                    "condition": "OnPropertyCondition", 
                    "message": "@ConditionalOnProperty (spring.devtools.remote.secret) did not find property 'secret'"
                }
            ], 
            "matched": [
                {
                    "condition": "OnClassCondition", 
                    "message": "@ConditionalOnClass found required classes 'javax.servlet.Filter', 'org.springframework.http.server.ServerHttpRequest'; @ConditionalOnMissingClass did not find unwanted class"
                }
            ]
        }
    }
}
```





## @Configuration

可理解为用spring的时候xml里面的<beans>标签

@Configuration中所有带@Bean注解的方法都会被动态代理，调用该方法返回的都是同一个实例