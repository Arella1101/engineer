# 顶级Kaggler如何比赛（2）

这是是Coursera课程[How to Win a Data Science Competition: Learn from Top Kagglers](https://www.coursera.org/learn/competitive-data-science/home/welcome)的笔记，本文是第二部分。

## 探索性数据分析（EDA）

### What and Why

- 更好的理解数据（better understand the data）
- 建立数据直觉（build an intuition about the data）
- 建立假设（generate hypothesizes）
- 获得洞察力（find insights）

千万不要刚开始就用复杂模型，比如stacking

**Visualizations**

![24](C:\dev\gitlab\engineer\img\顶级Kaggler如何比赛\24.JPG)

### 理解数据很重要

通过理解动机可以达到很高的准确率，例子如下

![25](C:\dev\gitlab\engineer\img\顶级Kaggler如何比赛\25.JPG)

（额，这是个leakage的例子，因为他还有promo，所以之后就会用）

**结论**

所以通过EDA，我们可以

- 熟悉数据
- 发现magic特征

记住，先EDA，不要急于摆模型

### 建立数据直觉

**领域知识很重要**

需要google，wikipedia等等各种知识，深入理解问题

比如：
任务：预测广告主的花费
数据：

|AdGroupId |AdNetwork Type2 |MaxCpc |Slot |Clicks |Impressions |… |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- |
|78db044136 |s |0.28 |s_2 |3 |0 |…|
| 68a0110c33 | s    | 1    | s_2  | 1    | 13   | …    |
| 2r39fw11w3 | p    | 1.2  | p_1  | 3    | 419  | …    |

看字段就发现这是google广告系统的任务，而且还能查到每个字段的详细意义。

**检查数据直觉意义**

知道每个字段意义之后，就可以检查字段有效性

比如：

| …    | Age     | …    |
| ---- | ------- | ---- |
| …    | 21      | …    |
| …    | 45      | …    |
| …    | **336** | …    |
| …    | 19      | …    |
| …    | …       | …    |

年龄这个字段为什么会是`336`？如果是人的年龄，那很可能是笔误，真实数据很可能是`36`，但如果不是人的，那要另做判断了

回到上面那个adword数据，`Impressions`怎么会比`Clicks`多呢？也许又是个bug

age的bug更像是数据错误，改掉就好了，adword的bug更像是逻辑错误，善加利用可能会获得更高的分数，比如可以加个Is_correct字段，帮助模型认识数据：
|AdGroupId |AdNetwork Type2 |MaxCpc |Slot |Clicks |Impressions |Is_correct|… |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- |
|78db044136 |s |0.28 |s_2 |3 |0 |False|…|
| 68a0110c33 | s    | 1    | s_2  | 1    | 13   |True| …    |
| 2r39fw11w3 | p    | 1.2  | p_1  | 3    | 419  |True| …    |

**理解数据如何生成**

非常重要，通过理解数据如何生成，才能生成更好的验证模式（validation schema）

如果训练和测试集是通过不同算法生成的，那随机抽取训练集的一部分作为验证集，肯定会产生偏差。

这个[notebook](https://gitlab.com/peikuo/Advanced-Machine-Learning-Coursera/blob/master/How-to-Win-a-Data-Science-Competition-Learn-from-Top-Kagglers/EDA_video2.ipynb)很好的说明了问题

有时候把数据画出来，可以看到分布有明显的偏差，这种问题一定要处理了：

![26](C:\dev\gitlab\engineer\img\顶级Kaggler如何比赛\26.JPG)

### 探索匿名数据（explore anonymized data）

举例：

| Text | Encoded text |
| ---- | ------------ |
| I want this table | 7ugy 972h 98ww hj34 |
| Table is what I want | hj34 4f08 rtte 7ugy 972h |
| This table is red | 98ww hj34 4f08 4rj9 |
| And this is me | jk8r 98ww 4f08 9jo4 |

或

|    id|x1    | x2   |x3    |   x4 |   x5 |   x6 |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- |
|1| m268i97y| 0| NO |105.4| 14|
|2 | j0gheu6 |1 |YES | 25.631 |12|
|3 |26fmsp6u |1 |NO |12.0 |12| m268i97y|
|4 |13e5dpzp |0| NO |140.12| 14| m268i97y|

出题者想要隐藏一些信息，就会使用匿名数据

**探索**

探索单个特征

- 猜测特征意义
- 猜测特征类型

探索特征关系

- 寻找一对关系
- 寻找特征组

[notebook](https://gitlab.com/peikuo/Advanced-Machine-Learning-Coursera/blob/master/How-to-Win-a-Data-Science-Competition-Learn-from-Top-Kagglers/EDA_video3_screencast.ipynb)讲解匿名特征，主要内容是根据数值特征各种实验猜测出最重要的匿名特征是**年份**

猜测数据类型的方法，因为每种类型都有自己的处理方法

```python
df.dtypes
df.info()
x.value_counts()
x.isnull()
```

如果类型是object，那就只能逐行慢慢看了

**可视化**

**EDA is an art! And visualizations are our art tools**

**探索单个特征**

- Histograms

  ```python
  plt.hist(x)
  ```

  看特征是unique还是有很多repeated

  histograms有欺骗性，不要只根据一个plot判断结果，也许经过算法处理，数据分布就很清晰了，如下

  处理前：

  ![27](C:\dev\gitlab\engineer\img\顶级Kaggler如何比赛\27.JPG)

  处理后：

  ![28](C:\dev\gitlab\engineer\img\顶级Kaggler如何比赛\28.JPG)

  处理过的数据和之前完全不一样了。而且观察处理过的数据，peak值明显有问题，好像是平局值，为什么会这么高，很有可能是缺失值被系统填充成平均值了。可以变回Nan，使用**XGBoost**处理或者加入not_null可能会有更好的效果。

- Plot

  index versus value

  ```python
  plt.plot(x,’.’)
  ```

  ![29](C:\dev\gitlab\engineer\img\顶级Kaggler如何比赛\29.JPG)

  如果横线很多，说明有很多重复值，并且是随机分布的

  index versus value by classes

  ```python
  plt.scatter(range(len(x)), x, c=y)
  ```

  ![30](C:\dev\gitlab\engineer\img\顶级Kaggler如何比赛\30.JPG)

  一看上图就知道数据没有充分shuffle

- Statistics

  ```python
  df.describe()
  x.mean()
  x.var()
  ```

- Other

  ```python
  x.value_counts()
  x.isnull()
  ```

  （讲的有点少，我自己补张表）

  | target / feature | Categorical                                               | Numerical                                  |
  | ---------------- | --------------------------------------------------------- | ------------------------------------------ |
  | **Categorical**  | Mosaic plots （*statsmodels.graphics.mosaicplot.mosaic*） | Box plots （*matplotlib.pyplot.boxplot* ） |
  | **Numerical**    | Density plots （*sns.distplot*）                          | Scatterplots                               |

**探索特征间关系**

- scatter

  ```python
  plt.scatter(x1, x2)
  ```

  ![31](C:\dev\gitlab\engineer\img\顶级Kaggler如何比赛\31.JPG)

  可以通过scatter判断训练集和测试集分布是否相同，如图：

  ![32](C:\dev\gitlab\engineer\img\顶级Kaggler如何比赛\32.JPG)

  上图明显不同，可能是什么地方处理有问题，要检查，要不然只能用线性模型判断

  看个特殊例子：

  ![33](C:\dev\gitlab\engineer\img\顶级Kaggler如何比赛\33.JPG)

  特征间的关系是: $X1 >= 1 - X2$，对于这种情况，可以考虑生成新特征，比如为tree model生成差值，比例等等

  全部scatter图列出来：

  ```python
  pd.scatter_matrix(df)
  ```

- correlation

  ```python
  df.corr(), plt.matshow( … )
  ```

  还可以计算多少多少个feature1大于feature2，特征组合之后再查看相关性等等

- group

  Corrplot + clustering

  ```python
  df.mean().sort_values().plot(style=’.’)
  ```

  ![34](C:\dev\gitlab\engineer\img\顶级Kaggler如何比赛\34.JPG)

### 数据集清理和其他检查

- Dataset cleaning

  **列去重：**

  查看有没有常量

  ```python
  traintest.nunique(axis=1) == 1
  ```

  ![35](C:\dev\gitlab\engineer\img\顶级Kaggler如何比赛\35.JPG)

  如果训练集和测试集有同样的常量（f0），删掉。如图不一样（f1），仔细观察为什么再考虑删掉

  ```pyh
  traintest.T.drop_duplicates()
  ```

  有一种去重是只有字母不一样，但都是按照相同规律出现的列，如（f4和f5），按照出现顺序重新label encode，再去重，如下：

  ```python
  for f in categorical_feats:
      traintest[f] =raintest[f].factorize()
  
  traintest.T.drop_duplicates()
  ```

  **行去重：**

  遇到相同row，去重。如果特征都一样，但target不一样，那可能是俄罗斯轮盘，结果可能非常随机，public和private leader board可能会有不同结果，小心处理。

  如果训练集和测试集都有很多重复数据，这可能会泄露数据生成方法，大胆假设小心求证

  ```python
  traintest.drop_duplicates()
  ```

- 检查数据是否shuffle

  ![36](C:\dev\gitlab\engineer\img\顶级Kaggler如何比赛\36.JPG)

  比较mean和rolling_mean，如果shuffle，rolling_mean应该在mean附近震荡。如图，数据最后一段就不正常，检查出了什么问题，可能会有数据泄露。

**总结**

发现的每一种数据不正常，都需要一个说法，也许可以从中发现规律或数据泄露，进而提高成绩

**Tools**

- [Seaborn](https://seaborn.pydata.org/)
- [Plotly](https://plot.ly/python/)
- [Bokeh](https://github.com/bokeh/bokeh)
- [ggplot](http://ggplot.yhathq.com/)
- [Graph visualization with NetworkX](https://networkx.github.io/)

- [Biclustering algorithms for sorting corrplots](http://scikit-learn.org/stable/auto_examples/bicluster/plot_spectral_biclustering.html)

讲一个[例子](https://gitlab.com/peikuo/Advanced-Machine-Learning-Coursera/blob/master/How-to-Win-a-Data-Science-Competition-Learn-from-Top-Kagglers/EDA_Springleaf_screencast.ipynb)，关于使用EDA处理**Springleaf**比赛的，上面说的都用到了

## Validation 

本节就是要教你如何正确的建立验证集

### Validation and overfitting

验证集帮助评估模型质量，主要是因为验证集可以验证哪个模型在没见到的数据上表现更好

overfitting和underfitting

![37](C:\dev\gitlab\engineer\img\顶级Kaggler如何比赛\37.JPG)

**Overfitting in general != overfitting in competitions **

实践中的overfitting意味着

- 捕获噪音
- 捕获的模式在测试集里不灵

比赛中的overfitting意味着

- 模型在测试集上质量不佳，而通过验证集却没有反应出来

### Validation strategies

- Holdout: ngroups = 1

  ```python
  sklearn.model_selection.ShuffleSplit
  ```

​	holdout适合数据分布均衡并数据量大的数据集。注意训练和测试集中不要有重复数据，这相当于泄露数据

- K-fold: ngroup = k

  ```python
  sklearn.model_selection.Kfold
  ```

  数据量很大的话，计算量特别大，需要平衡。

- Leave-one-out: ngroups = len(train)

  ```python
  sklearn.model_selection.LeaveOneOut
  ```

  计算量非常大，数据少合适

**Stratification**

切割数据后的分布，要和切割前一致。Stratification就是为了保证多folder下分布一致。

用处：

- 数据量小
- 数据集不平衡
- 多种类分类

### Data splitting strategies

一般数据切分有两种方法：随机和按时间切分

分割数据的最终目标：验证集和测试集越像越好

如果我们仔细按照时间序列生成特征，但使用随机方法切分出验证集，效果会好吗？

不会！

![38](C:\dev\gitlab\engineer\img\顶级Kaggler如何比赛\38.JPG)

你看上图左，如果train和validation是随机分隔的，那最后预测的值很可能是train和validation的平均值。而右侧用时间切分就没有这个问题。

不同的验证分隔策略很明显影响：

- 生成特征
- 模型如何依赖特征
- 可能会有点target leak

**三个方案：**

- Random, rowwise

  row互相之间没有关系

  但是如果相关，比如说一个家族，爸爸在训练集，妈妈在测试集，那很可能可以开发出一个新特征，用于提高成绩

- Timewise

  特定时间之前的数据在训练，之后的在测试。

  也可以生成新特征哦，比如预测下个月卖多少钱，那就加一个上个月平均价格，上个月平均客户数等等，很常用很好用

  **Moving window validation: **

  ![39](C:\dev\gitlab\engineer\img\顶级Kaggler如何比赛\39.JPG)

- By ID




