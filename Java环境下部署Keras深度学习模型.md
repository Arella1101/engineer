# [译]Java环境下部署Keras深度学习模型

本文翻译自[***Deploying Keras Deep Learning Models with Java***](https://towardsdatascience.com/deploying-keras-deep-learning-models-with-java-62d80464f34a)，侵删。

Keras库提供了容易上手的深度学习接口，帮助更多的人实现神经网络，但我面对的一个挑战是如何把Keras中实现的model变成产品。Keras是python实现的，至今对其他语言也没提供多少支持。虽然Flask，PySpark，CloudML提供了可以让这些python模型产品化，但我倾向于用Java来部署model。

尽管业内有[ONNX](https://github.com/onnx/onnx)这样致力于将深度学习标准化的project，但这些方案提供的运行环境太有限了。有个办法是将Keras model转化成TensorFlow Graphs，然后在支持TensorFlow的环境中使用这个graph。

最近我发现Deeplearning4J (DL4J)可以直接把keras模型部署在java深度学习运行环境，太赞了。

[Keras Import Overview | Deeplearning4j](https://deeplearning4j.org/docs/latest/keras-import-overview)

我的用例是使用Keras探索深度学习模型，然后通过Java把模型产品化。如果是将模型部署在client上（例如Android设备），或者是在已有Java系统上部署模型，这个方案是非常好的。将Keras模型部署在DL4J的介绍在[这](https://deeplearning4j.org/docs/latest/keras-import-overview)。

本文是Java环境下Keras模型的概览。我用**Jetty**做实时预测，用**Google’s DataFlow**构建批量预测系统，[GitHub](https://github.com/bgweber/DeployKeras/tree/master)上有运行这些例子的完整代码和数据。

（上面都是废话。。。）

### 训练模型

第一步是用Keras库训练模型，一旦训练好准备部署，保存成**H5**格式，在**Java**和**Python**应用中使用。本教程会使用该Model预测哪些玩家会买新游戏。过去我在利用Flask部署深度学习模型中用过。

[Deploying Keras Deep Learning Models with Flask](https://towardsdatascience.com/deploying-keras-deep-learning-models-with-flask-5da4181436a2)

Model的输入是十个二进制特征（*G1, G2, … ,G10*），这些特征是描述玩家买过的游戏。标签（Label）是单变量，描述用户是否买了游戏。训练过程的主要步骤如下：

```python
import keras
from keras import models, layers
# Define the model structure
model = models.Sequential()
model.add(layers.Dense(64, activation='relu', input_shape=(10,)))
...
model.add(layers.Dense(1, activation='sigmoid'))
# Compile and fit the model
model.compile(optimizer='rmsprop',loss='binary_crossentropy',
              metrics=[auc])
history = model.fit(x, y, epochs=100, batch_size=100,
                    validation_split = .2, verbose=0)
# Save the model in h5 format 
model.save("games.h5")
```

我把训练结果保存在H5文件里，用于之后部署的Python和Java应用。过去我曾经用***Flask***来做实时预测，这次我会用Java构建批量预测系统。

### Java Setup

我会用***Deeplearning4j***来部署Keras模型，用Dataflow来做批量预测，Jetty做实时预测，厦下面是我使用的库：

- [**Deeplearning4j**](https://deeplearning4j.org/)：Java神经网络框架
- [**ND4J**](https://nd4j.org/)：Java 张量（tensor）库
- [**Jetty**](https://www.eclipse.org/jetty/)：Java Web应用
- [**Cloud DataFlow:**](https://cloud.google.com/dataflow/)：GCP平台上提供自动扩容的批量预测服务

使用***pom.xml***组织工程，为了使用Keras，***DL4J***的***core***和***modelimport***库都要用。

```xml
<dependencies>
  <dependency>      
    <groupId>org.deeplearning4j</groupId>      
    <artifactId>deeplearning4j-core</artifactId>
    <version>1.0.0-beta2</version>    
  </dependency>         
  <dependency>      
    <groupId>org.deeplearning4j</groupId>      
    <artifactId>deeplearning4j-modelimport</artifactId>      
    <version>1.0.0-beta2</version>    
  </dependency>                       
  <dependency>      
    <groupId>org.nd4j</groupId>      
    <artifactId>nd4j-native-platform</artifactId>
    <version>1.0.0-beta2</version>    
  </dependency>
  <dependency>      
    <groupId>org.eclipse.jetty</groupId>      
    <artifactId>jetty-server</artifactId>      
    <version>9.4.9.v20180320</version>   
  </dependency>  <dependency>      
    <groupId>com.google.cloud.dataflow</groupId>      
    <artifactId>google-cloud-dataflow-java-sdk-all</artifactId>  
    <version>2.2.0</version>     
  </dependency>
</dependencies>
```

在Eclipse里构建项目，只要***pom***配好，不要增加其他配置了。

### Keras Predictions with DL4J

环境配置好，我们开始Keras模型预测，下面的代码用加载好的Keras模型预测一组采样数据，首先加载H5文件，接着定义长度为10的1D张量，用随机生成的二进制数据赋值。最后调用Keras模型的***output***方法生成预测值。因为模型只有单值输出，我用***getDouble(0)***返回结果。

```java
// imports
import org.deeplearning4j.nn.modelimport.keras.KerasModelImport;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.io.ClassPathResource;
// load the model
String simpleMlp = new ClassPathResource(
                          "games.h5").getFile().getPath();
MultiLayerNetwork model = KerasModelImport.
                    importKerasSequentialModelAndWeights(simpleMlp);
// make a random sample
int inputs = 10;
INDArray features = Nd4j.zeros(inputs);
for (int i=0; i<inputs; i++) 
    features.putScalar(new int[] {i}, Math.random() < 0.5 ? 0 : 1);
// get the prediction
double prediction = model.output(features).getDouble(0);
```

因为Java没有内建的高效张量（***tensor***）计算库，所以要使用DL4J来操作张量。DL4J提供了N维数组作为Java深度学习的后端。只需要把整型数组和索引放入张量就可以了。

模型对象提供了*predict*和*output*方法，*predict*方法返回类别（class predict，值为0或1），*output*方法返回连续变量，即类别几率，类似**scikit-learn**的***predict_proba***。

### 实时预测（Real-Time Predictions）

Keras模型可以在Java中运行了，现在我们启动预测服务。第一步启动Jetty提供Web入口，在之前的帖子里（[tracking data](https://towardsdatascience.com/data-science-for-startups-tracking-data-4087b66952a1)和[model production](https://towardsdatascience.com/data-science-for-startups-model-production-b14a29b2f920)）我详细介绍了如何运行Jetty。完整代码在[这里](https://github.com/bgweber/DeployKeras/blob/master/JettyDL4J.java)。

Keras模型的入口用一个单类实现，这个类实现了接口*AbstractHandler*返回模型结果，下面的代码实现了如何在端口8080运行Jetty服务器，并在**JettyDL4J**类里加载Keras模型。

```java
// Setting up the web endpoint
Server server = new Server(8080);
server.setHandler(new JettyDL4J());
server.start();
server.join();
// Load the Keras model 
public JettyDL4J() throws Exception {  
    String p=new ClassPathResource("games.h5").getFile().getPath(); 
    model=KerasModelImport.importKerasSequentialModelAndWeights(p); 
}
```

下面的***Handler***代码片段来处理web请求。将传入的参数（G1, G2, ..., G10）转换成1D张量，输入给Keras模型，返回预测值。Request请求被标记成已处理并返回字符串结果。

```java
// Entry point for the model prediction request 
public void handle(String target,Request baseRequest, 
    HttpServletRequest request, HttpServletResponse response) 
    throws IOException, ServletException {
  // create a dataset from the input parameters
  INDArray features = Nd4j.zeros(inputs);
  for (int i=0; i<inputs; i++) 
      features.putScalar(new int[] {i},  Double.parseDouble(
                          baseRequest.getParameter("G" + (i + 1))));
  // output the estimate
  double prediction = model.output(features).getDouble(0);
  response.setStatus(HttpServletResponse.SC_OK);
  response.getWriter().println("Prediction: " + prediction);
  baseRequest.setHandled(true);
}
```

运行这个类，他会监听端口8080。可以通过下面的URL查看返回结果。

```
// Request
http://localhost:8080/?G1=1&G2=0&G3=1&G4=1&G5=0&G6=1&G7=0&G8=1&G9=1&G10=1
// Result
Prediction: 0.735433042049408
```

URL返回的就是Keras模型的实时预测结果。如果是生产系统，最好在Jetty前置Http服务，而不要直接把Jetty暴露在最前面。

### 批量预测（Batch Predictions）

另外一个用例是用Keras模型做批量预测，这样可以预测百万条记录。当然可以直接通过Python Keras模型预测，但是能力受限。我来演示如何使用***Google's DataFlow***来预测海量数据。之前的帖子我介绍了如何使用***DataFlow***（[model production](https://towardsdatascience.com/data-science-for-startups-model-production-b14a29b2f920)和[game simulations](https://towardsdatascience.com/scaling-game-simulations-with-dataflow-172926612d50)）。

使用***DataFlow***，可以在数据集上制定一系列操作，数据源和目标可以是关系型数据库，消息服务（*message service*），应用数据库或其他服务。当一组操作被应用于批量操作，我们可以迅速增加基础服务处理大量数据，处理好再关闭服务。或者使用流式数据，基础服务一直等待数据请求，来一条处理一条。两种情境下，服务都可以根据需求自动扩容而不惧大量数据计算的挑战。

![11](/img/Java环境下部署Keras深度学习模型/11.png)

*DataFlow*的***DAG***流程如上图所示。首先创建数据集，本例中，我从CSV文件里加载数据，实践上我通常用***BigQuery***来加载和同步数据。下一步是用***TableRow***作为输入，转成1D张量，喂给模型，输出TableRow作为预测结果。完整代码在[这里](https://github.com/bgweber/DeployKeras/blob/master/DataFlowDL4J.java)。

*Keras*预测的核心代码如下。转换操作（A transformation operates）将对象集合转化成输出集合。你可以在转换器中使用Keras模型。注意Keras模型只会加载一次不会用一次加载一次，效率很高。

```java
// Apply the transform to the pipeline
.apply("Keras Predict", new PTransform<PCollection<TableRow>, 
                                       PCollection<TableRow>>() {       
  // Load the model in the transformer
  public PCollection<TableRow> expand(PCollection<TableRow> input) {                        
    final int inputs = 10;
    final MultiLayerNetwork model;       
    try {        
     String p= newClassPathResource("games.h5").getFile().getPath();
     model=KerasModelImport.importKerasSequentialModelAndWeights(p);
  }             
  catch (Exception e) {
     throw new RuntimeException(e);
  }
  // create a DoFn for applying the Keras model to instances  
  return input.apply("Pred",ParDo.of(new DoFn<TableRow,TableRow>(){
    @ProcessElement
    public void processElement(ProcessContext c) throws Exception {
       ...  // Apply the Keras model
    }}));         
}})
```

处理数据元素的代码如下，首先读取记录，从***TableRow***里读取数据创建张量并传入模型，最好保存结果，结果行里保存预测值和实际值。

```java
  // get the record to score
  TableRow row = c.element();      
  // create the feature vector                   
  INDArray features = Nd4j.zeros(inputs);                 
  for (int i=0; i<inputs; i++)                   
    features.putScalar(new int[] {i}, 
          Double.parseDouble(row.get("G" + (i+1)).toString()));                                              
  // get the prediction                  
  double estimate = model.output(features).getDouble(0);              
  // save the result                  
  TableRow prediction = new TableRow();                    
  prediction.set("actual", row.get("actual"));                  
  prediction.set("predicted", estimate);                        
  c.output(prediction);
```

想要保存结果到***BigQuery***，需要设置***tempLocation***参数如下：

```
--tempLocation=gs://your-gs-bucket/temp-dataflow-location
```

跑完这个DAG，BigQuery中会生存新的表，包含预测和实际结果。下图的实例。

![11](/img/Java环境下部署Keras深度学习模型/22.png)

### 结论

随着深度学习越发重要，越来越多的编程语言和环境开始支持深度学习模型。随着深度学习模型格式的进一步标准化，使得训练和部署运行分开变得更加容易可行。本文介绍了如何通过DL4J将Keras模型部署在Java环境中，用于实时预测和批量预测。终于我们可以让深度模型批量处理百万数据。

（这篇写得太啰嗦了。。。）