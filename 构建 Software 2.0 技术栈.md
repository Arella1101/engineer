# 构建 Software 2.0 技术栈

本文根据**Andrej Karpathy**在TRAIN AI 2018上[演讲](https://www.youtube.com/watch?v=zywIvINSlaI)整理而成

这次演讲的核心理念是：如果engineer system是software 1.0，那software 2.0就是learning system。

### Engineer System

先看engineer system技术栈怎么做（使用任务分解方法）：

1. 定义问题
2. 把大问题分解成小问题
3. 为每一个单独问题设计算法
4. 把解决方案组合成系统（这就是技术栈stack）

如下图，有AWS的stack，TCP/IP的stack和Android的stack：

![1](/img/构建 Software 2.0 技术栈/1.png)

这是1.0的，但是这要如何解决图像识别问题呢？

历程：

![2](/img/构建 Software 2.0 技术栈/2.png)

![3](/img/构建 Software 2.0 技术栈/3.png)

### Learning System

这些都是过去做图像识别的方法，不过苦日子都过去了，因为我们有了深度学习

![4](/img/构建 Software 2.0 技术栈/4.png)

通过神经网络训练图像识别任务比之前的特征抽取轻松太多了

随着神经网络推进，更多的结构被推新出来

![5](/img/构建 Software 2.0 技术栈/5.png)

准确度更高了

![6](/img/构建 Software 2.0 技术栈/6.png)

如果没有数据，那神经网络比不上传统工程，但是数据越多，神经网络效果越好，最后会大大超越传统方法。

Software 2.0怎么做？如下

![7](/img/构建 Software 2.0 技术栈/7.png)

类似于auto ml了，设计代码骨架，评估性能，自动迭代

未来Software 1.0和2.0并不是替换关系，而是共存，有编程的部分，但可能有一些复杂的，通过常规手段无法解决的问题，要通过数据和优化器来解决，如图：

![8](/img/构建 Software 2.0 技术栈/8.png)

现在已经有很多模型来处理各种问题，如图：

![9](/img/构建 Software 2.0 技术栈/9.png)

现在Google用C++做系统，未来可能就是一个能解决所有问题的大模型来做就行了。

波士顿动力的机器人也是传统工程，因为里面没有机器学习或者神经网络（真的吗？）。

同时，1.0也是2.0的基础，大量的基础工程需要用传统的方法实现，如下图：

![10](/img/构建 Software 2.0 技术栈/10.png)

### Softwear 2.0的优势

- Computationally homogeneous（同质化计算）

  比方说**Resnet**，只有卷积和池化，没有乱哄哄的代码，结构非常清晰

- Hardware-friendly

- Constant running time and memory use

- Agile

  如果你想提升系统处理能力两倍但保证结果差距不大，传统方法是做不到的，神经网络可以，只要拿到一些channel重新训练就好

- Finetuning

  一点代码都不要改，只需选择要重新训练的层就好

- works very well

### Software 2.0 在 Tesla

制动和加速系统

之前：

![11](/img/构建 Software 2.0 技术栈/11.png)

现在：

![12](/img/构建 Software 2.0 技术栈/12.png)

驻车：

![13](/img/构建 Software 2.0 技术栈/13.png)

### Programing with 2.0 stack

如果优化器可以做大部分coding，那还需要人做什么？

两件事：

- 标记数据
- 维护数据集基础设施
  - 标记labeler不同意见, 统计labeler数据，‘升级’特征（Flag labeler disagreements, keep stats on labelers, “escalation” features）
  - 标记‘有趣的’数据，因为有些数据很少，但是很有用，scale it
  - 数据清理
  - 数据可视化

读博士主要精力花在算法，tesla主要精力在数据：![14](/img/构建 Software 2.0 技术栈/14.png)

### 重要的经验

- 正确的标记数据是非常重要的

  举了自动驾驶的例子，看到的图片千奇百怪，很难标记好

- 找出标签/数据不平衡是非常重要的

  比如说90%的车图片是轿车，0.003%的车图片是巴士，那巴士的数据分布要提升，要不然神经网络会任务不重要

- 标记数据是一个持续迭代的过程

  比如自动雨刮器，正常只要探测是不是有水在玻璃上就好了，但是上线后发现有霜/灰/光晕/隧道等情况雨刮器会判断错误（如下图），收集到新情况要持续标记数据，重新训练

  ![15](/img/构建 Software 2.0 技术栈/15.png)

### 最重要的问题

为software 2.0准备的工具集还不存在呢（很少人意识到这点）！

下面是1.0的IDE：

![16](/img/构建 Software 2.0 技术栈/16.png)

而2.0的还没有！（juptyer太弱？）

下图是2.0的IDE应该做到的事情：

![17](/img/构建 Software 2.0 技术栈/17.png)