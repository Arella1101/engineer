# Parallel, Concurrent, and Distributed Programming in Java笔记

本文基于Coursera课程[Parallel, Concurrent, and Distributed Programming in Java](https://www.coursera.org/specializations/pcdp)，课程比较简单，快的话两周都搞定，讲课的是个胖胖的墨西哥老头，呵呵

## Distributed

- MapReduce

  - Map to group same key
  - Reduce to operate same key

- Hadoop

  - write large-scale parallel programs that operate on this “big data”
  - automatic fault-tolerance
  - word-count example

- Spark

  - in-memory computing within distributed nodes
  - Resilient Distributed Dataset (RDD),  a generalization of sets of key-value pairs
  - intermediate operations called Transformations (e.g., map,filter,join,...)
  - terminal operations called Actions (e.g., reduce,count,collect,...)
  - intermediate transformations are performed lazily

- TF-IDF

  - 

    ![img](https://img.mubu.com/document_image/992b2d3b-cd72-4848-917a-cdbeb0f65cfd-775812.jpg)

- PAGE RANK

  - 

    ![img](https://img.mubu.com/document_image/109a9c56-ecc2-48af-9455-9f7d43e05b65-775812.jpg)

  - join(), flatMapToPair(), reduceByKey() and mapValues()

- Socket

  - Server
    - ServerSocket
    - socket.accept()
    - s.getInputStream() / s.getOutputStream()
  - Client
    - Socket
    - s.getInputStream() / s.getOutputStream()

- Serialization and Deserialization

  - transient

- Remote Method Invocation (RMI)

  - x = y.rmi(), x and y all need serializable

- Multicast Sockets

  - multicast sockets enable a sender to efficiently send the same message to a specified set of receivers on the Internet.
  - MulticastSocket
  - message 64kb limited
  - join() / leave()

- Publish-Subscribe Pattern

  - advantage 
    - publishers need not be aware of which processes are the subscribers, and vice versa
    - very efficient implementation
      because it can enable a number of communication optimizations, which include batching and topic partitioning across broker nodes.
      ​​
    - improve reliability
      because broker nodes can replicate messages in a topic, so that if one node hosting a topic fails, the entire publish-subscribe system can continue execution with another node that contains a copy of all messages in that topic
    - KafkaProducer.send() / KafkaConsumer.poll()

- Single Program Multiple Data (SPMD) Model

  - Message Passing Interface (MPI)

    - Init

      - MPI_Init()
      - MPI_Comm_size(mpi.MPI_COMM_WORLD)
      - MPI_Comm_rank(mpi.MPI_COMM_WORLD)
      - MPI_Finalize()

    - Point-to-Point Communication

      <https://en.wikipedia.org/wiki/Message_Passing_Interface#Example_program>

      - MPI_Send()
      - MPI_Recv()

    - Message Ordering and Deadlock

    - Non-Blocking Communications

      - MPI_Irecv()
      - MPI_Isend()
      - MPI_Wait()
      - MPI_Waitall()

    - Collective Communication

      - MPI_Bcast()
      - MPI_Reduce() 

- Combining Distribution and Multithreading

  - a process corresponds to a single Java Virtual Machine (JVM) instance
    - advantage
      - improved responsiveness
      - improved scalability
      - improved resilience to JVM failures within a node
  - threads are created within a JVM instance
    - advantage
      - increased sharing of memory and per-process resources by threads
      - improved responsiveness
      - improved performance

- MPI and Multithreading

  - MPI_THREAD_FUNNELED
  - MPI_THREAD_SERIALIZED
  - MPI_THREAD_MULTIPLE

- Distributed Actors

- Distributed Reactive Programming

  - Flow.Publisher
  - Flow.Subscriber
  - Flow.Process
  - Flow.Subscription
  - key benefit of reactive programming is that the programmer can control the "batching'' of information between the publisher and the subscribe

## Parallel

- Task-level Parallelism

  - Task Creation and Termination (Async, Finish)
    finish {
      async S1; // asynchronously compute sum of the lower half of the array
      S2;       // compute sum of the upper half of the array in parallel with S1
    }
    S3; // combine the two partial sums after both S1 and S2 have finished

  - Fork/Join Framework

    - RecursiveAction
      - fork()
      - join()
      - invokeAll()
        注意，不要对第一个任务做fork，直接调用compute()，具体参考invokeAll()实现.

- Computation Graphs, Work, Span, Ideal Parallelism

  - Computation Graphs (CGs)
    - A set of vertices or nodes, in which each node represents a step consisting of an arbitrary sequential computation.
    - A set of directed edges that represent ordering constraints among steps
  - Fork–Join programs
    - Continue edges that capture sequencing of steps within a task.
    - Fork edges that connect a fork operation to the first step of child tasks.
    - Join edges that connect the last step of a task to all join operations on that task.
  - WORK(G)
    - the sum of the execution times of all nodes in CG G
  - SPAN(G) 
    - the length of a longest path in G, the longest paths are known as critical paths

- Multiprocessor Scheduling, Parallel Speedup

  -  Tp as the execution time of a CG on P processors

  - 

    ![img](https://img.mubu.com/document_image/08e2769d-5413-4585-81ff-ad0df038cfcf-775812.jpg)

  - 

    ![img](https://img.mubu.com/document_image/875e1e4c-ae8e-4e21-ba62-c8635c62f1aa-775812.jpg)

  - Speedup(P) must be ≤ the number of processors P , and also ≤ the ideal parallelism, WORK/SPAN

- Amdahl’s Law

  - q ≤ 1 is the fraction of WORK in a parallel program that must be executed sequentially
  - Speedup(P) ≤ 1/q
    就是说不能并行部分会成为并发瓶颈，如果q=0.2，那么最多快5倍
    ​

- Future Tasks

  - Future tasks are tasks with return values
  - two key operations
    - Assignment
      - future { ⟨ task-with-return-value ⟩ }
    - Blocking read
      - future.get()

- Creating Future Tasks in Java’s Fork/Join Framework

  - RecursiveTask
    - compute() method of a future task must have a non-void return type
      RecursiveAction's compute() method do not have return value
    - task.join()provide return value

- Functional Parallelism

  - Memoization
  - Java Streams
    students.stream()
        .filter(s -> s.getStatus() == Student.ACTIVE)
        .map(a -> a.getAge())
        .average();
    students.parallelStream() / Stream.of(students).parallel()​
  - Determinism and Data Races
    - A parallel program is said to be functionally deterministic if it always computes the same answer when given the same input
    - structurally deterministic always computes the same computation graph, when given the same input
    - “benign”nondeterminism for programs with data races

- Loop Parallelism

  - forall (i : [0:n-1]) a[i] = b[i] + c[i]
  - IntStream.rangeClosed(0, N-1).parallel().toArray(i -> b[i] + c[i]);
  - Tutorial on Executing Streams in Parallel
    <https://docs.oracle.com/javase/tutorial/collections/streams/parallelism.html#executing_streams_in_parallel>
  - Barriers
  - Chunking
  - Phaser
    - ph.arriveAndAwaitAdvance() = Barrier
    - ph.arrive()
    - ph.awaitAdvance()

## Concurrent

-  Java Threads

  - 

    ![img](https://img.mubu.com/document_image/c570eaa1-0842-450b-b1fc-56a143a1ae80-775812.jpg)

  - wait operation in the form of a join() method

- Structured Locks

  - synchronized
    - mutual exclusion and avoid data races
      - synchronized(object)
      - synchronized method = synchronized(this)
  - wait() / notify() / notifyAll()
    wait()必须在synchronized中使用，前提是synchronized那个object, wait()会释放当前锁，直到notify() / notifyAll()会唤醒，让其他线程抢夺锁。
    notify()通知1个线程，notifyAll()通知所有线程。​​

- Unstructured Locks

  - ReentrantLock()

    - lock() / unlock()
    - tryLock()
    - lockInterruptibly()

  - ReentrantReadWriteLock()

    - L.readLock().lock() / L.writeLock().lock()

  - make sure to call unlock() at the end

  - volatile

    - 数据载入过程

      ![img](https://img.mubu.com/document_image/199f07d1-186a-4a04-b8a3-0da695971aaf-775812.jpg)

    - 原子性

      - lock、unlock、read、load、assign、use、store、write必须是原子
      - JVM自行实现单个指令原子性保证

    - 可见性

      - 一个共享变量的更改，其他线程是可以立即看到修改后的值

    - 有序性

      - 如果在本线程内观察，所有的操作都是有序的；如果在一个线程中观察另一个线程，所有的操作都是无序的。前半句指“线程内表现为串行的语义”（as-if-serial），后半句指“指令重排序”和普通变量的”工作内存与主内存同步延迟“的现象(store-write的中间可能被其他指令插队了，而导致的延迟)。

    - 4种内存屏障

      - LoadLoad 屏障
        执行顺序：Load1—>Loadload—>Load2
        确保Load2及后续Load指令加载数据之前能访问到Load1加载的数据。
      - StoreStore 屏障
        执行顺序：Store1—>StoreStore—>Store2
        确保Store2以及后续Store指令执行前，Store1操作的数据对其它处理器可见。
      - LoadStore 屏障
        执行顺序： Load1—>LoadStore—>Store2
        确保Store2和后续Store指令执行前，可以访问到Load1加载的数据。
      - StoreLoad 屏障
        执行顺序: Store1—> StoreLoad—>Load2
        确保Load2和后续的Load指令读取之前，Store1的数据对其他处理器是可见的。

    - Lock前缀的总线CPU锁

      - Lock会对CPU总线和高速缓存加锁,lock上锁后会执行数据更新，把CPU缓存的数据写回主内存，同时让其他CPU缓存的数据失效而从主内存重新读取。

    - volatile它的作用

      - 保证有序性
        　　1）当程序执行到volatile变量的读操作或者写操作时，在其前面的操作的更改肯定全部已经进行，且结果已经对后面的操作可见；在其后面的操作肯定还没有进行；
        　　2）在进行指令优化时，不能将在对volatile变量访问的语句放在其后面执行，也不能把volatile变量后面的语句放到其前面执行。
      - 插入storeload内存屏障，阻止指令并行重排序优化。
      - CPU修改变量的值前，发送cpu lock指令，锁住cpu缓存的变量数据和主内存数据，同步新变量值至主内存，同时使其他CPU缓存的该变量失效，再释放锁。下次使用时其他CPU将直接从主内存读取（基于MESI）
      - 可以保证单指令的原子性和多线程间的可见性，但是无法保证复合操作的原子性

- Liveness and Progress Guarantees

  - deadlock
    - all threads are blocked indefinitely, thereby preventing any forward progress
  - livelock
    - all threads repeatedly perform an interaction that prevents forward progress
  - starvation
    - at least one thread is prevented from making any forward progress
  - Dining Philosophers Problem

- Critical Sections

  - data race error

- Object-Based Isolation

  - Example
    - delete node in LinkedList
      isolated(cur, cur.prev, cur.next, () -> {
          . . . // Body of object-based isolated construct
      });
    - Spanning Tree Example

- Atomic Variables

  - getAndAdd() / compareAndSet()

- Concurrent Data Collection

  - HashMap problem
    - 并发情况下put可能会发生死循环和CPU 100%，原因是发生环形列表，在map扩容rehash时，挂起会行程环形列表，导致死循环。

- Read-Write Isolation

- Thread Pool

  - ForkJoinPool

    不是每个 fork() 都会促成一个新线程被创建，而每个 join() 也不是一定会造成线程被阻塞
    ForkJoinPool 的每个工作线程都维护着一个工作队列（WorkQueue），这是一个双端队列（Deque），里面存放的对象是任务（ForkJoinTask）。
    每个工作线程在运行中产生新的任务（通常是因为调用了 fork()）时，会放入工作队列的队尾，并且工作线程在处理自己的工作队列时，使用的是 LIFO 方式，也就是说每次从队尾取出任务来执行。
    每个工作线程在处理自己的工作队列同时，会尝试窃取一个任务（或是来自于刚刚提交到 pool 的任务，或是来自于其他工作线程的工作队列），窃取的任务位于其他线程的工作队列的队首，也就是说工作线程在窃取其他工作线程的任务时，使用的是 FIFO 方式。
    在遇到 join() 时，如果需要 join 的任务尚未完成，则会先处理其他任务，并等待其完成。
    在既没有自己的任务，也没有可以窃取的任务时，进入休眠。​

    - fork
      把任务推入当前工作线程的工作队列里
    - join
      检查调用 join() 的线程是否是 ForkJoinThread 线程。如果不是（例如 main 线程），则阻塞当前线程，等待任务完成。如果是，则不阻塞。
      查看任务的完成状态，如果已经完成，直接返回结果。
      如果任务尚未完成，但处于自己的工作队列内，则完成它。
      如果任务已经被其他的工作线程偷走，则窃取这个小偷的工作队列内的任务（以 FIFO 方式），执行，以期帮助它早日完成欲 join 的任务。
      如果偷走任务的小偷也已经把自己的任务全部做完，正在等待需要 join 的任务时，则找到小偷的小偷，帮助它完成它的任务。
      递归地执行第5步。

  - ThreadPoolExecutor

- Actor Model

  - Actor consists
    - a Mailbox
    - a set of Methods
    - Local State
  - Reactive
    - Only execute methods in response to messages; these methods can read/write local state and/or send messages to other actors
  - AKKA
  - Pipeline Parallelism
    - Use actor as pipeline on processing message
  - Producer-Consumer Problem with Unbounded Buffer
    - producer tasks, consumer tasks, and the shared buffer, use as actor
    - Unbounded Buffer
      - PriorityBlockingQueue
      - SynchronousQueue
      - ConcurrentLinkedQueue
    - Bounded Buffer
      - ArrayBlockingQueue

- Optimistic Concurrency

  - use compareAndSet()on while parse
  - Concurrent Queues
    def enq(obj) 
    ​while(true) 
    if(queue.next.compareAndSet(​null, obj)) 
    return;
  - Linearizability
    A single object is considered linearizable if
    (a) each of its methods is atomic. Imagine them as java synchronized methods, but more below.
    (b) there can be at most one pending operation from any given thread/processor.
    (c) the operations must take effect before they return. It is not acceptable for the object to enqueue them to perform them lazily.
    不太好解释，意思就是对一个q，先enq a, 再enq b，这两个方法返回后deq，应该返回a或者b，不可能返回空
  - Lock optimizer
    - 减少持锁时间
    - 减少锁粒度
      - ConcurrentHashMap
    - 锁分离
      - 读写
    - 锁粗化
      不停请求，同步，释放不利于优化
    - JVM优化
      - 锁偏向
      - 轻量级锁
      - 自旋锁
      - 锁消除
    - ThreadLocal
    - No Lock
      - CAS
      - AtomicInteger...
      - sun.misc.Unsafe
  - 模式
    - 单例
    - 不变
    - 生产-消费者模式
      - BlockQueue
      - 无锁
        - Disruptor
          - RingBuffer
    - Future