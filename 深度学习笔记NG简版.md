# 深度学习笔记NG简版

## 神经网络与深度学习

- Notation

  - 

    ![img](https://img.mubu.com/document_image/c36f9f67-5579-4e54-8bcc-b80107ef5816-775812.jpg)

- 逻辑回归(Logistic Regression)

  - Sigmoid

    - 

      ![img](https://img.mubu.com/document_image/350966c2-9743-405e-a27e-e2c2aa2997e9-775812.jpg)

  - Logistic Regression Cost Function

    - Output function

      - 

        ![img](https://img.mubu.com/document_image/d600448d-4187-4a16-ae0d-320c4e3fc340-775812.jpg)

    - Loss function

      - 

        ![img](https://img.mubu.com/document_image/b0d75d4f-51e7-49b6-bd7a-b4f26890c8c7-775812.jpg)

    - Gradient Descent

      - J(w, b) 是一个凸函数(convex function)

      - 

        ![img](https://img.mubu.com/document_image/1c067857-570e-4935-8d19-0d2d3db117cf-775812.jpg)

      - a 表示学习率

    - 导数（Derivatives）

      - Examples

        - 

          ![img](https://img.mubu.com/document_image/92a6800d-9214-49c5-bd18-c5e12050ef16-775812.jpg)

      - 导数就是斜率，而函数的斜率，在不同的点是不同的

      - 函数的导数在网上查

    - 计算图（Computation Graph）

      - 

        ![img](https://img.mubu.com/document_image/a143b7af-b91d-4af4-b9c6-c2d1affe2452-775812.jpg)

    - Logistic Regression Gradient Descent

      - 

        ![img](https://img.mubu.com/document_image/586c8705-d483-4f42-8b0b-7d9b08cde3c8-775812.jpg)

      - 

        ![img](https://img.mubu.com/document_image/24251d6b-6ab6-44d6-b8f9-e600b5e53dbe-775812.jpg)

    - m 个样本的梯度下降(Gradient Descent on m Examples)

      - 

        ![img](https://img.mubu.com/document_image/08226d29-bbf2-4c00-b44a-add81408ae70-775812.jpg)


        J=0;
        ​dw1=0;
        ​dw2=0;
        ​db=0;
        for i = 1 to m
        ​   z(i) = wx(i)+b;   
        ​a(i) = sigmoid(z(i));
        ​   J += -[y(i)log(a(i))+(1-y(i)）log(1-a(i));
        ​   dz(i) = a(i)-y(i);
        ​   dw1 += x1(i)dz(i);
        ​   dw2 += x2(i)dz(i);
        ​   db += dz(i);
        J/= m;dw1/= m;
        ​dw2/= m;
        db/= m;
        ​w=w-alpha*dw
        ​b=b-alpha*db

  - 向量化(Vectorization)

    - 

      ![img](https://img.mubu.com/document_image/cb996534-ca14-4c3a-8ff8-33e9b1c175ba-775812.jpg)

  - Broadcasting in Python

    - 

      ![img](https://img.mubu.com/document_image/f5a645dd-2081-4197-88cd-1fe2fa407ba7-775812.jpg)

  - Explanation of logistic regression cost function

    - 

      ![img](https://img.mubu.com/document_image/6f2a2e52-8a25-4468-ad65-f66de2a9d21e-775812.jpg)

- Shallow neural networks

  - Neural Network Representation

    - 

      ![img](https://img.mubu.com/document_image/29b3c435-0ee1-420d-9be8-e2d7d6757744-775812.jpg)

  - Activation functions

    - sigmoid

      - 

        ![img](https://img.mubu.com/document_image/3e9ea669-8e71-4670-8af3-363da67cb242-775812.jpg)

      - 0~1区间

      - 

        ![img](https://img.mubu.com/document_image/c690aa14-52d1-475f-87ac-3b85c3c5673b-775812.jpg)

      - 导数

        - 

          ![img](https://img.mubu.com/document_image/1935ebf7-7b1b-4740-81ce-97d34f9776cc-775812.jpg)

      - 除了输出层是一个二分类问题基本不会用它

    - tanh

      - 

        ![img](https://img.mubu.com/document_image/5e5a7a95-8a49-4918-ab69-a007f3f9c3c9-775812.jpg)

      - -1~1区间，效果优于sigmoid

      - 

        ![img](https://img.mubu.com/document_image/31339d99-5c7f-4b69-961f-cff9e992e746-775812.jpg)

      - 导数

        - 

          ![img](https://img.mubu.com/document_image/4a13def2-2099-4243-a0b3-655dbe74e173-775812.jpg)

      - tanh是非常优秀的，几乎适合所有场合

    - relu / leaky relu

      - 

        ![img](https://img.mubu.com/document_image/518ad1c9-9353-4cbb-ba1c-89421334e57b-775812.jpg)

      - 

        ![img](https://img.mubu.com/document_image/d3e4e1e4-9923-4ee5-9dc8-730c58018236-775812.jpg)

      - 最常用的默认函数，，如果不确定用哪个激活函数，就使用ReLu或者Leaky ReLu

    - 在z的区间变动很大的情况下，激活函数的导数或者激活函数的斜率都会远大于0，在程序实现就是一个if-else语句，而sigmoid函数需要进行浮点四则运算，在实践中，使用ReLu激活函数神经网络通常会比使用sigmoid或者tanh激活函数学习的更快。

    - sigmoid和tanh函数的导数在正负饱和区的梯度都会接近于0，这会造成梯度弥散，而Relu和Leaky ReLu函数大于0部分都为常熟，不会产生梯度弥散现象。(同时应该注意到的是，Relu进入负半区的时候，梯度为0，神经元此时不会训练，产生所谓的稀疏性，而Leaky ReLu不会有这问题)

  - why need a nonlinear activation function

    - 没有激活函数，输出y只是输入x特征的线性组合

  - Random+Initialization

    - w初始化成0，由于所有的隐含单元都是对称的，无论你运行梯度下降多久，他们一直计算同样的函数(symmetry breaking problem)
    - b没有这个问题，可以初始化成0
    - w用高斯分布初始化很好，np.random.randn(2,2) * 0.01
      - 为什么乘以0.01
        如果很大，那么你很可能最终停在（甚至在训练刚刚开始的时候）很大的值，这会造成tanh/Sigmoid激活函数饱和在龟速的学习上，如果你没有sigmoid/tanh激活函数在你整个的神经网络里，就不成问题

- Deep Neural Network

  - Deep L-layer neural network

  - Forward and backward propagation

    - 

      ![img](https://img.mubu.com/document_image/2992949f-23a7-4885-bd57-69e4a4af2dfd-775812.jpg)

    - 

      ![img](https://img.mubu.com/document_image/f2ec0276-72cf-4909-9671-05e21a9aec8c-775812.jpg)

    - 

      ![img](https://img.mubu.com/document_image/441db667-594b-49f4-b51c-9e4542fab5e5-775812.jpg)

  - Forward propagation in a Deep Network

    - 

      ![img](https://img.mubu.com/document_image/ac64c98e-36e3-41fb-9d74-545aebacc179-775812.jpg)

  - Getting your matrix dimensions right

    - w的维度是（下一层的维数，前一层的维数）
    - b的维度是（下一层的维数，1）

  - Why deep representations?

    - 较早的前几层能学习一些低层次的简单特征，等到后几层，就能把简单的特征结合起来，去探测更加复杂的东西

  - Building blocks of deep neural networks

    - 

      ![img](https://img.mubu.com/document_image/78ec73cd-0827-4e16-8220-ccd3c86d74b9-775812.jpg)

  - Parameters vs Hyperparameters

    - learning rate a （学习率）
    - iterations（梯度下降法循环的数量）
    - L（隐藏层数目）
    - n[l]（隐藏层单元数目）
    - choice of activation function（激活函数的选择）
    - momentum
    - mini batch size
    - regularization parameters

  - How to find the best hyperparameters

    - 

      ![img](https://img.mubu.com/document_image/6c2c9348-b518-4378-ada8-a1fbcb53d734-775812.jpg)

  - What does this have to do with the brain

    - 

      ![img](https://img.mubu.com/document_image/ad7e9daa-69f5-4102-876f-f9b6ff0fb281-775812.jpg)

## Improving Deep Neural Networks:Hyperparameter tuning, Regularization and Optimization

- Practical aspects of Deep Learning

  - Train / Dev / Test sets

    - 传统机器学习
      - 70%验证集，30%测试集
      - 60%训练，20%验证和20%测试集
    - 大数据时代
      - 测试集的主要目的是正确评估分类器的性能，所以，如果拥有百万数据，我们只需要1000条数据，便足以评估单个分类器，并且准确评估该分类器的性能。
      - 数据量过百万的应用，训练集可以占到99.5%，验证和测试集各占0.25%，或者验证集占0.4%，测试集占0.1%。
      - 确保验证集和测试集的数据来自同一分布
    - 训练验证集
      就算没有测试集也不要紧，测试集的目的是对最终所选定的神经网络系统做出无偏估计，如果不需要无偏估计，也可以不设置测试集。所以如果只有验证集，没有测试集，我们要做的就是，在训练集上训练，尝试不同的模型框架，在验证集上评估这些模型，然后迭代并选出适用的模型。

  - Bias /Variance

    - 假设这数据集拟合一条直线，可能得到一个逻辑回归拟合，但它并不能很好地拟合该数据，这是高偏差（high bias）的情况，我们称为“欠拟合”（underfitting）。

    - 相反的如果我们拟合一个非常复杂的分类器，比如深度神经网络或含有隐藏单元的神经网络，可能就非常适用于这个数据集，但是这看起来也不是一种很好的拟合方式分类器方差较高（high variance），数据过度拟合（overfitting）。

    - 在两者之间，可能还有一些像图中这样的，复杂程度适中，数据拟合适度的分类器，这个数据拟合看起来更加合理，我们称之为“适度拟合”（just right）是介于过度拟合和欠拟合中间的一类。

    - 

      ![img](https://img.mubu.com/document_image/29b6906b-6411-4c33-9824-16b76cc08734-775812.jpg)

    - 例子

      - 假定训练集误差是1%，为了方便论证，假定验证集误差是11%，可以看出训练集设置得非常好，而验证集设置相对较差，我们可能过度拟合了训练集，在某种程度上，验证集并没有充分利用交叉验证集的作用，像这种情况，我们称之为“高方差”。
      - 假设训练集误差是15%，我们把训练集误差写在首行，验证集误差是16%，假设该案例中人的错误率几乎为0%，人们浏览这些图片，分辨出是不是猫。算法并没有在训练集中得到很好训练，如果训练数据的拟合度不高，就是数据欠拟合。
      - 再举一个例子，训练集误差是15%，偏差相当高，但是，验证集的评估结果更糟糕，错误率达到30%，在这种情况下，我会认为这种算法偏差高，因为它在训练集上结果不理想，而且方差也很高，这是方差偏差都很糟糕的情况。
      - 再看最后一个例子，训练集误差是0.5%，验证集误差是1%，用户看到这样的结果会很开心，猫咪分类器只有1%的错误率，偏差和方差都很低。
      - 这些分析都是基于假设预测的，假设人眼辨别的错误率接近0%，一般来说，最优误差也被称为贝叶斯误差，所以，最优误差接近0%，我就不在这里细讲了，如果最优误差或贝叶斯误差非常高，比如15%。我们再看看这个分类器（训练误差15%，验证误差16%），15%的错误率对训练集来说也是非常合理的，偏差不高，方差也非常低。

  - Basic Recipe for Machine Learning

    - 如果bias较高，试着评估训练集或训练数据的性能。如果偏差的确很高，甚至无法拟合训练集，那么你要做的就是选择一个新的网络，比如含有更多隐藏层或者隐藏单元的网络，或者花费更多时间来训练网络，或者尝试更先进的优化算法。
    - 如果variance高，最好的解决办法就是采用更多数据，如果你能做到，会有一定的帮助，但有时候，我们无法获得更多数据，我们也可以尝试通过正则化来减少过拟合。
    - Notice
      - 高偏差和高方差是两种不同的情况，我们后续要尝试的方法也可能完全不同，我通常会用训练验证集来诊断算法是否存在偏差或方差问题，然后根据结果选择尝试部分方法。举个例子，如果算法存在高偏差问题，准备更多训练数据其实也没什么用处
      - 在机器学习的初期阶段，关于所谓的偏差方差权衡的讨论屡见不鲜，原因是我们能尝试的方法有很多。可以增加偏差，减少方差，也可以减少偏差，增加方差，但是在深度学习的早期阶段，我们没有太多工具可以做到只减少偏差或方差却不影响到另一方。但在当前的深度学习和大数据时代，只要持续训练一个更大的网络，只要准备了更多数据，那么也并非只有这两种情况，我们假定是这样，那么，只要正则适度，通常构建一个更大的网络便可以，在不影响方差的同时减少偏差，而采用更多数据通常可以在不过多影响偏差的同时减少方差。这两步实际要做的工作是：训练网络，选择网络或者准备更多数据。
      - Regularization可以训练一个更大的网络几乎没有任何负面影响，而训练一个大型神经网络的主要代价也只是计算时间，前提是网络是比较规范化的。

  - Regularization

    - 解决高方差的办法

      - 正则化
      - 增加数据

    - 正则化

      - L2正则化

        - 

          ![img](https://img.mubu.com/document_image/cb7c30a7-d237-4124-9840-236819947b8d-775812.jpg)

        - 权重衰减

          - 

            ![img](https://img.mubu.com/document_image/b66ba764-4513-4dc4-85c0-58856df64bcb-775812.jpg)

      - L1正则化

        - 

          ![img](https://img.mubu.com/document_image/c30ff11b-282c-464b-b05b-79a0642d0316-775812.jpg)

        - 如果用的是正则化，最终会是稀疏的，也就是说向量中有很多0，有人说这样有利于压缩模型，因为集合中参数均为0，存储模型所占用的内存更少。实际上，虽然正则化使模型变得稀疏，却没有降低太多存储内存，所以我认为这并不是正则化的目的，至少不是为了压缩模型，人们在训练网络时，越来越倾向于使用正则化。

      - Why regularization reduces overfitting

        - 如果lambda设置的足够大，W会接近0，实际上不会发生这种情况，不过很多神经元会失去作用，这样神经网络变简单了，导致不容易发生过拟合。
        - 如果正则化参数变得很大，参数W很小，也会相对变小，此时忽略的影响，会相对变小，实际上，激活函数的取值范围很小，这个激活函数，也就是曲线函数tanh会相对呈线性，整个神经网络会计算离线性函数近的值，这个线性函数非常简单，并不是一个极复杂的高度非线性函数，不会发生过拟合。

    - Dropout Regularization

      - 随机砍掉一些节点，让神经网络变得更小，实现正则化。

      - 实现

        - 砍点

          ![img](https://img.mubu.com/document_image/c84f4041-5dd4-4b74-b0d6-3ed68620d049-775812.jpg)

        - 扩展a（Inverted dropout）

          ![img](https://img.mubu.com/document_image/832086a9-8f0f-459a-9acc-8257a4b0dc1a-775812.jpg)

          - 除以keep-prob是为了不改变期望E

      - 测试阶段不使用dropout

      - Understanding Dropout

        - dropout的功能类似于正则化，与正则化不同的是，被应用的方式不同，dropout也会有所不同，甚至更适用于不同的输入范围。
        - 实施dropout，在计算机视觉领域有很多成功案例
        - dropout一大缺点就是代价函数J不再被明确定义，每次迭代，都会随机移除一些节点，如果再三检查梯度下降的性能，实际上是很难进行复查的

    - Other regularization methods

      - 数据扩增
        - 人工合成
          - 水平翻转图片
          - 随意裁剪图片
        - 强变形处理
      - early stopping
        - 缺点
          - 不能独立地处理这两个问题，因为提早停止梯度下降，也就是停止了优化代价函数，因为现在你不再尝试降低代价函数J，所以代价函数的值可能不够小，同时你又希望不出现过拟合，你没有采取不同的方式来解决这两个问题，而是用一种方法同时解决两个问题，这样做的结果是我要考虑的东西变得更复杂。
        - 优点
          - 只运行一次梯度下降，你可以找出的较小值，中间值和较大值，而无需尝试正则化超级参数的很多值。
        - 如果不用early stopping，另一种方法就是L2正则化，训练神经网络的时间就可能很长。我发现，这导致超级参数搜索空间更容易分解，也更容易搜索，但是缺点在于，你必须尝试很多正则化参数的值，这也导致搜索大量值的计算代价太高。不过感觉还是比early stopping好啊

    - Normalizing inputs

      - 零均值化

        - 

          ![img](https://img.mubu.com/document_image/7dc2dc9c-80b5-4f34-8fbc-f785bd8c8fbe-775812.jpg)

      - 归一化方差

        - 

          ![img](https://img.mubu.com/document_image/6d19a20c-197e-45ec-8305-d175d61ed211-775812.jpg)

      - 

        ![img](https://img.mubu.com/document_image/257324e2-c6b7-45f6-b597-fb70e769bca5-775812.jpg)

      - 所以如果输入特征处于不同范围内，可能有些特征值从0到1，有些从1到1000，那么归一化特征值就非常重要了。如果特征值处于相似范围内，那么归一化就不是很重要了

    - Vanishing / Exploding gradients

      - 级数，导致1.1变无穷大，0.9变0

    - Weight Initialization for Deep NetworksVanishing / Exploding gradients

      - 原理：n越大，w越小
        z = w1x1 + w2x2 + ... + wnxn, 为了预防值过大或过小，你可以看到越大，你希望越小，因为是的和。
        ​
      - He initializationfor relu
        - 均值为0，方差为sqrt(2/n)的w
          np.random.randn(layers_dims[l], layers_dims[l - 1]) * np.sqrt(2. / layers_dims[l - 1])
      - Xavier initializationfor tanh
        - 保持输入和输出的方差一致，这样就避免了所有输出值都趋向于0，方差为sqrt(1/n)
          np.random.randn(layers_dims[l], layers_dims[l - 1]) * np.sqrt(1. / layers_dims[l - 1])

    - Numerical approximation of gradients

      - 双边误差

        ![img](https://img.mubu.com/document_image/63fab7f4-268f-473d-82df-7099c9f0e4db-775812.jpg)

        比较准确

    - Gradient checking

      - Need future study

- Optimization algorithms

  - Mini-batch gradient descent

    - 

      ![img](https://img.mubu.com/document_image/c89f93f3-5fd0-46fb-937c-57296d0ff56b-775812.jpg)

    - 如果训练集较小，直接使用batch梯度下降法（小于2000个样本）

    - 样本数目较大的话，一般的mini-batch大小为64到512，mini-batch大小是2的次方，代码会运行地快一些

    - mini-batch中，要确保和要符合CPU/GPU内存

  - Exponentially weighted averages

    - theta指数加权移动平均值

      ![img](https://img.mubu.com/document_image/6dd62c49-a302-4723-8ec9-a8564e0371c3-775812.jpg)

    - 其实1 / (1 - theta)为关注的次数，比如theta=0.9时，其实是关注10天的变化

    - 好处是省内存，只需要一个公式不断覆盖

  - Bias correction in exponentially weighted averages

    - 初期时候，移动平均值非常不准，把初始值换成

      ![img](https://img.mubu.com/document_image/19f069fc-7cd2-4848-935d-09f5fb6ea48d-775812.jpg)

    - 对比

      ![img](https://img.mubu.com/document_image/7a4ecacd-7ccb-49b5-874a-1d684d6094c5-775812.jpg)

    - 其实也可以不设置，只要挺过开始这段，后面就好啦

  - Gradient descent with Momentum

    - 在纵轴上，你希望学习慢一点，因为你不想要这些摆动，但是在横轴上，你希望加快学习，你希望快速从左向右移，移向最小值，移向红点。

      ![img](https://img.mubu.com/document_image/1b4a68a8-90a3-4aa1-ba16-1ba4b80bc564-775812.jpg)

    - 两个超参数，学习率alpha以及参数theta，theta控制着指数加权平均数。最常用的值是0.9

      ![img](https://img.mubu.com/document_image/cfd2f834-c1fe-4347-a291-f8fdc43f1001-775812.jpg)

  - RMSprop

    - 如果你执行梯度下降，虽然横轴方向正在推进，但纵轴方向会有大幅度摆动，目标就是减缓纵轴方向，同时加快，至少不是减缓横轴方向的学习

      ![img](https://img.mubu.com/document_image/22ba56e8-f474-4a54-929f-0b728324576b-775812.jpg)

    - 算法步骤

      ![img](https://img.mubu.com/document_image/cfe545ff-b0ad-4aa1-859e-9cb12de35aac-775812.jpg)

    - 适合处理非平稳目标，对RNN很好用

  - Adam optimization algorithm

    - Momentum和RMSprop结合在一起

      ![img](https://img.mubu.com/document_image/4f22478f-e226-4b94-99cc-8fcf90b255c7-775812.jpg)

    - 算法步骤

      ![img](https://img.mubu.com/document_image/ab52121f-8844-4f1e-b91c-7a88c85c5f2d-775812.jpg)

    - 超参选择

      ![img](https://img.mubu.com/document_image/19d54b4e-cd8d-4704-a5a7-b9f2fde17677-775812.jpg)

    - 优点

      - 对内存需求较小
      - 为不同的参数计算不同的自适应学习率
      - 也适用于大多非凸优化- 适用于大数据集和高维空间

  - Learning rate decay

    慢慢减少alpha的本质在于，在学习初期，你能承受较大的步伐，但当开始收敛的时候，小一些的学习率能让你步伐小一些

    - 最普通的，每个epoch减少1/n

      ![img](https://img.mubu.com/document_image/a79ea372-4f86-42cc-8ccd-4eabe60ae676-775812.jpg)

    - 手动调整

    - 

      ![img](https://img.mubu.com/document_image/2253a09d-eeb0-4f37-8563-70171fbf1cd6-775812.jpg)

  - The problem of local optima

    - 学习缓慢，adam等成熟算法会帮助走出saddle

- Hyperparameter tuning

  - Tuning process
    - try random value, not use grid
      - 从粗到细，从细到粗
        比如在二维的那个例子中，你进行了取值，也许你会发现效果最好的某个点，也许这个点周围的其他一些点效果也很好，那在接下来要做的是放大这块小区域，然后在其中更密集得取值或随机取值，聚集更多的资源，在这个蓝色的方格中搜索，如果你怀疑这些超参数在这个区域的最优结果，那在整个的方格中进行粗略搜索后，你会知道接下来应该聚焦到更小的方格中。在更小的方格中，你可以更密集得取点。这种从粗到细的搜索也经常使用。
  - Using an appropriate scale to pick hyperparameters
    - 如搜索超参数alpha（学习速率），取值从0.0001~1，不能均匀采样，需要使用对数取值，很均匀。
    - 如beta，取值从0.9~0.999, 使用1-beta的对数取值

- Hyperparameters tuning in practice: Pandas vs. Caviar

  - panda tuning 只有一个模型，慢慢调，慢慢看。Caviar tuning 鱼子酱，生成很多模型，看看那个好。

    ![img](https://img.mubu.com/document_image/cc46b1b9-2544-4eaa-90ec-485dd71c6c07-775812.jpg)

  - 两种方式的选择，是由你拥有的计算资源决定的。

- Normalizing activations in a network

  - Batch归一化会使你的参数搜索问题变得很容易，使神经网络对超参数的选择更加稳定，超参数的范围会更加庞大，效果更好。

    ![img](https://img.mubu.com/document_image/72933670-df32-49f9-8c84-38095693202e-775812.jpg)

    - 通过变换将输入变成均值0，方差1的输入，数值会在激活函数微分最陡峭的地方，加速学习

  - Fitting Batch Norm into a neural network

    - 

      ![img](https://img.mubu.com/document_image/b9fe5df1-b371-4d73-9a9d-6921015a3454-775812.jpg)

  - Batch Norm at test time

    - 训练得到的mu和sigma，用于测试集
      常运用指数加权平均来追踪在训练过程中的mu和sigma

  - Why does Batch Norm work

    - 通过归一化让输入均值0方差1
    - 减少了输入值改变的问题，它的确使这些值变得更稳定，防止“梯度弥散”
      减少了这些隐藏值分布变化的数量，它可以使权重比你的网络更滞后或更深层
    - 它有轻微的正则化效果
    - 神经网络训练时遇到收敛速度很慢，或梯度爆炸等无法训练的状况时可以尝试BN来解决

  - BP

    ![img](https://img.mubu.com/document_image/f4dc230c-a20b-4edc-b29f-1afdce09f62e-775812.jpg)

- Softmax regression

  - 

    ![img](https://img.mubu.com/document_image/e7edb479-dde2-4986-b10b-4d240938c9d7-775812.jpg)

  - 

    ![img](https://img.mubu.com/document_image/31a3541d-2251-4c5e-9115-ed1cd3840581-775812.jpg)

  - 

    ![img](https://img.mubu.com/document_image/9abfa1b4-e82b-4dfb-907d-370e65cc2563-775812.jpg)

## Structuring Machine Learning Projects

- Why ML Strategy

  - 分析机器学习问题的方法

- Orthogonalization

  - 正交化，隔离性
    正交化的意思就是设计一些旋钮，他们之间都是90度，都是basis都有明确的功能，调整一个不影响另外的属性。
    早期停止，我觉得早期停止有点难以分析，因为这个旋钮会同时影响你对训练集的拟合，因为如果你早期停止，那么对训练集的拟合就不太好，但它同时也用来改善开发集的表现，所以这个旋钮没那么正交化。因为它同时影响两件事情。​

- 要想模型好，需要保证四件事

  - 你通常必须确保至少系统在训练集上得到的结果不错，所以训练集上的表现必须通过某种评估，达到能接受的程度，对于某些应用，这可能意味着达到人类水平的表现
    如果你的算法在成本函数上不能很好地拟合训练集，你想要一个旋钮，是的我画这东西表示旋钮，或者一组特定的旋钮，这样你可以用来确保你的可以调整你的算法，让它很好地拟合训练集，所以你用来调试的旋钮是你可能可以训练更大的网络，或者可以切换到更好的优化算法，比如Adam优化算法
  - 在训练集上表现不错之后，希望系统能在开发集上有好的表现
    你的算法在开发集上做的不好，它在训练集上做得很好，但开发集不行，然后你有一组正则化的旋钮可以调节，尝试让系统满足第二个条件。类比到电视，就是现在你调好了电视的宽度，如果图像的高度不太对，你就需要一个不同的旋钮来调节电视图像的高度，然后你希望这个旋钮尽量不会影响到电视的宽度。增大训练集可以是另一个可用的旋钮，它可以帮助你的学习算法更好地归纳开发集的规律，现在调好了电视图像的高度和宽度。
  - 系统在测试集上也有好的表现
    如果系统在开发集上做的很好，但测试集上做得不好呢？如果是这样，那么你需要调的旋钮，可能是更大的开发集。因为如果它在开发集上做的不错，但测试集不行这可能意味着你对开发集过拟合了，你需要往回退一步，使用更大的开发集
  - 真实世界中表现不错
    如果它在测试集上做得很好，但无法给你的猫图片应用用户提供良好的体验，这意味着你需要回去，改变开发集或成本函数。因为如果根据某个成本函数，系统在测试集上做的很好，但它无法反映你的算法在现实世界中的表现，这意味着要么你的开发集分布设置不正确，要么你的成本函数测量的指标不对

- Single number evaluation metric

  - F1 score，综合precise和recall

    ![img](https://img.mubu.com/document_image/172d8eda-9e4e-45ac-8dbe-6fb0385304c3-775812.jpg)

  - 对不同区域做平均值

    ![img](https://img.mubu.com/document_image/14e3a850-b824-47ff-8df9-b40b2aa542d6-775812.jpg)

- Satisficing and optimizing metrics

  - 如果你需要顾及多个指标，比如说，有一个优化指标，你想尽可能优化的，然后还有一个或多个满足指标，需要满足的，需要达到一定的门槛。现在你就有一个全自动的方法，在观察多个成本大小时，选出"最好的"那个。现在这些评估指标必须是在训练集或开发集或测试集上计算或求出来的

    ![img](https://img.mubu.com/document_image/a62b7e66-7304-49ef-989b-f4b88b1330ca-775812.jpg)

- Train/dev/test distributions

  - 开发集和测试集千万不要来自不同的分布
    议的是你将所有数据随机洗牌，放入开发集和测试集
    在设立开发集和测试集时，要选择这样的开发集和测试集，能够反映你未来会得到的数据，认为很重要的数据，必须得到好结果的数据，特别是，这里的开发集和测试集可能来自同一个分布​

- Size of dev and test sets

  - 98%作为训练集，1%开发集，1%测试集

- When to change dev/test sets and metrics

  - some target is much more important than others, than add weights on this.
    比如黄色图片不能有

- Why human-level performance

  ![img](https://img.mubu.com/document_image/d5978550-0c0b-4dbd-a1d0-363d81374ceb-775812.jpg)

- Improving your model performance

  - 评估的基石

    ![img](https://img.mubu.com/document_image/848f224c-c642-44da-9784-572ca258e7fb-775812.jpg)

  - reduce bias and variance

    ![img](https://img.mubu.com/document_image/dba37932-3cc1-4c80-b7f0-376e3adc3446-775812.jpg)

    bias: 使用规模更大的模型，这样算法在训练集上的表现会更好，或者训练更久。使用更好的优化算法，比如说加入momentum或者RMSprop，或者使用更好的算法，比如Adam。你还可以试试寻找更好的新神经网络架构，或者说更好的超参数。这些手段包罗万有，你可以改变激活函数，改变层数或者隐藏单位数，虽然你这么做可能会让模型规模变大。或者试用其他模型，其他架构，如循环神经网络和卷积神经网络
    variance: 你可以收集更多数据，因为收集更多数据去训练可以帮你更好地推广到系统看不到的开发集数据。你可以尝试正则化，包括正则化，dropout正则化或者我们在之前课程中提到的数据增强。同时你也可以试用不同的神经网络架构，超参数搜索​

  - Carrying out error analysis
    不明白

  - Cleaning up Incorrectly labeled data

    ![img](https://img.mubu.com/document_image/aaab25e1-2430-4ae3-abdb-861d3f530e52-775812.jpg)

  - Training and testing on different distributions

  - Bias and Variance with mismatched data distributions

    ![img](https://img.mubu.com/document_image/35256c67-84b9-4860-b755-2063eace28c1-775812.jpg)

  - Addressing data mismatch

    - 数据合成

- Transfer learning

  - 用训练好的model，重新训练最后一层或几层，用于训练新的目标

    ![img](https://img.mubu.com/document_image/a21df87c-91c6-4301-9746-7f56e2125c59-775812.jpg)

    如果你重新训练神经网络中的所有参数，那么这个在图像识别数据的初期训练阶段，有时称为预训练（pre-training），因为你在用图像识别数据去预先初始化，或者预训练神经网络的权重。然后，如果你以后更新所有权重，然后在放射科数据上训练，有时这个过程叫微调（fine tuning）

  - 什么时候可以用

    ![img](https://img.mubu.com/document_image/46c963d8-4797-43a8-989e-23fc232f2bdf-775812.jpg)

- Multi-task learning

  - 在多任务学习中，你是同时开始学习的，试图让单个神经网络同时做几件事情，然后希望这里每个任务都能帮到其他所有任务

  - 多维向量y 不再是1个1，其他0，而是多个0

    ![img](https://img.mubu.com/document_image/bd506eb9-a628-4020-a620-0428f85c63da-775812.jpg)

  - Loss Function

    ![img](https://img.mubu.com/document_image/b937607e-c63d-482d-a522-61be58f5cf49-775812.jpg)

    注意累加，这是对多个目标求和

  - 何时有用

    ![img](https://img.mubu.com/document_image/cb62d5fb-dbbf-45e2-8a57-f0b644bac541-775812.jpg)

    1.如果果你训练的一组任务，可以共用低层次特征。对于无人驾驶的例子，同时识别交通灯、汽车和行人是有道理的，这些物体有相似的特征，也许能帮你识别停车标志，因为这些都是道路上的特征。
    ​2.数据量接近
    3.​当你可以训练一个足够大的神经网络，同时做好所有的工作，所以多任务学习的替代方法是为每个任务训练一个单独的神经网络

- End-to-end deep learning

  以前有一些数据处理系统或者学习系统，它们需要多个阶段的处理。那么端到端深度学习就是忽略所有这些不同的阶段，用单个神经网络代替它

  - Pros and Cons

    ![img](https://img.mubu.com/document_image/df6b5d28-d734-41a1-86a6-e80373df45b9-775812.jpg)

    ![img](https://img.mubu.com/document_image/7720ee9a-2c11-44ee-aeee-da5adbce1d0e-775812.jpg)

## Convolutional Neural Networks



- 卷积运算

  - 边缘检查

  - Padding

    - 不用padding的缺点
      - 图片越来越小
      - 边缘只被检查一次
    - 参数
      - Valid
      - Same

  - Strided convolutions

    - 不能整除，向下取整

      ![img](https://img.mubu.com/document_image/8ee44fc9-8f27-41c2-8f1a-e493dc54f8c3-775812.jpg)

  - 总结

    ![img](https://img.mubu.com/document_image/3d98c10b-c74f-4089-92ac-7b28e8724395-775812.jpg)

    ![img](https://img.mubu.com/document_image/5085276e-f2d3-4021-8e4c-3e8e6376b74a-775812.jpg)

    ![img](https://img.mubu.com/document_image/77f809ba-a1ad-47f8-b82d-31b8a92838cc-775812.jpg)

    参数数量计算：每一层都是一个3×3×3的矩阵，因此每个过滤器有27个参数，也就是27个数。然后加上一个偏差，现在参数增加到28个。现在我们有10个过滤器，加在一起是28×10，也就是280个参数

- Convolutional neural network example

  ![img](https://img.mubu.com/document_image/e55acac6-8ea6-4c0d-bbd3-4d541a5a14a9-775812.jpg)

  ![img](https://img.mubu.com/document_image/a4d3ad94-0c10-4118-9dfe-688fb5d14090-775812.jpg)

- Why convolutions?

  ![img](https://img.mubu.com/document_image/f8225b5a-9142-464e-9638-56f971057e8f-775812.jpg)

  卷积神经网络善于捕捉平移不变

- Case studies

  - Classic network

    - LeNet-5

      ![img](https://img.mubu.com/document_image/20a387f0-19ee-468e-886d-d2818defad2f-775812.jpg)

    - AlexNet

      ![img](https://img.mubu.com/document_image/1e6f6950-dfb6-4f47-8e02-34131d9f00a9-775812.jpg)

      AlexNet比LeNet大很多，使用RELU

    - VGG

      ![img](https://img.mubu.com/document_image/6b806c3f-7529-4e22-acbc-3ce8ed27274c-775812.jpg)

      这是一种只需要专注于构建卷积层的简单网络。首先用3×3，步幅为1的过滤器构建卷积层，padding参数为same卷积中的参数。然后用一个2×2，步幅为2的过滤器构建最大池化层。
      ​

  - Residual Networks (ResNets)

    ![img](https://img.mubu.com/document_image/329a972c-b452-47ea-a0f3-e1b3ac8d0204-775812.jpg)

    - 每两层增加一个捷径，构成一个残差块

      ![img](https://img.mubu.com/document_image/0c14ff0a-f52c-4d86-ab39-49b95462cf99-775812.jpg)

    - Why ResNets work?

      ![img](https://img.mubu.com/document_image/08827708-be68-4d67-a8c8-3f564ec4148d-775812.jpg)

      - 增加网络深度，不影响表达效果，学习不会消失
      - 残差块学习恒等函数非常容易，你能确定网络性能不会受到影响，很多时候甚至可以提高效率
      - 为了保证z[l]和a[l]有相同的维度，使用了很多same卷积

    - Network in Network and 1×1 convolutions

      - 降维（ dimension reductionality ）
        比如，一张500 * 500且厚度depth为100 的图片在20个filter上做1*1的卷积，那么结果的大小为500*500*20。
      - 加入非线性
        卷积层之后经过激励层，1*1的卷积在前一层的学习表示上添加了非线性激励（ non-linear activation ），提升网络的表达能力

  - Inception network

    - Inception network motivation

      ![img](https://img.mubu.com/document_image/801b80bc-cfa6-4787-ad79-fad95ab4e1ef-775812.jpg)

      如果你在构建神经网络层的时候，不想决定池化层是使用1×1，3×3还是5×5的过滤器，那么Inception模块就是最好的选择。我们可以应用各种类型的过滤器，只需要把输出连接起来

    - bottleneck layer, 先用1x1压缩，再用interceptor加大，有效降低参数数量

      ![img](https://img.mubu.com/document_image/75b1b22f-3b0b-472d-9455-ed98b9c1a75a-775812.jpg)

  - Interceptor network module

    ![img](https://img.mubu.com/document_image/6df57077-47df-45e2-8717-8f7b46fed7b3-775812.jpg)

  - Interceptor network

    ![img](https://img.mubu.com/document_image/c7a43096-83b9-4bb4-80d5-67c8141dc45c-775812.jpg)

    所以Inception网络只是很多这些你学过的模块在不同的位置重复组成的网络

  - Using open-source implementations

- Transfer Learning use existed model

- Data augmentation

  - 垂直镜像对称
  - 彩色转换
  - 失真

- Benchmark

  ![img](https://img.mubu.com/document_image/ec109145-9fb6-4f1c-a2de-b47ab9846eab-775812.jpg)

- Object detection

  - Object localization

    - 定义目标标签 y

      ![img](https://img.mubu.com/document_image/31ee24c9-c65a-4ab3-a765-a3d5adfbda83-775812.jpg)

      如果图片中存在定位对象，而y1=1，所以y1 = Pc，损失值就是不同元素的平方和，如果y1=0，损失值就是square(Yhat-Y1)

  - Landmark detection

    - 比如人脸，第一位是是否是人脸（0,1），剩余64位是关键点的位置，比如说眼睛鼻子啥啥的（x1，y1）

  - Object detection

    - 滑动窗口目标检测
      - 窗口大小
      - stride大小
      - 缺点
        - 计算成本
        - 卷积提高了计算效率

  - Convolutional implementation of sliding windows

    ![img](https://img.mubu.com/document_image/be2a8324-0cd6-4c97-ba76-1d1665d424af-775812.jpg)

    - 16*16*3的图片生成2*2*4的结果，这相当于14*14*3的窗口扫描图片生成1*1*4的结果4次，这是卷积的好处

  - Bounding box predictions

    - YOLO(You only look once)

      - 网格（比如19*19）
      - y = [Pc, Bx, By, Bh, Bw, C1, C2, C3]（结果概率）
        c1, c2, c3是需要判断的三个类别，Pc是[0, 1]有无，B是定位
      - 输入图片（608*608），使用19*19的网格，stride设成19，这样第一步切分出32个结果，通过DNN，最后生成(m, 19, 19, 5, 结果概率)，5是archor box

    - Intersection over union

      - IoU>=0.5，检测正确

        ![img](https://img.mubu.com/document_image/42144953-5ed3-4727-aa19-9f6a1296eb59-775812.jpg)

        IoU衡量了两个边界框重叠地相对大小。如果你有两个边界框，你可以计算交集，计算并集，然后求两个数值的比值，所以这也可以判断两个边界框是否相似

    - Non-max suppression

      ![img](https://img.mubu.com/document_image/84df7c16-9440-461e-a05f-e4a03d98e9a2-775812.jpg)

      非最大值意味着你只输出概率最大的分类结果，但抑制很接近，但不是最大的其他预测结果，所以这方法叫做非极大值抑制

    - Anchor Boxes

      让一个格子检测出多个对象

      - 算法

        ![img](https://img.mubu.com/document_image/b36f2980-b6ce-40e3-b2c1-60f1aa749298-775812.jpg)

        现在用到anchor box这个概念，是这么做的。现在每个对象都和之前一样分配到同一个格子中，分配到对象中点所在的格子中，以及分配到和对象形状交并比最高的anchor box中。所以这里有两个anchor box，你就取这个对象，如果你的对象形状是这样的（编号1，红色框），你就看看这两个anchor box，anchor box 1形状是这样（编号2，紫色框），anchor box 2形状是这样（编号3，紫色框），然后你观察哪一个anchor box和实际边界框（编号1，红色框）的交并比更高，不管选的是哪一个，这个对象不只分配到一个格子，而是分配到一对，即（grid cell，anchor box）对，这就是对象在目标标签中的编码方式。所以现在输出  就是3×3×16，上一张幻灯片中你们看到  现在是16维的，或者你也可以看成是3×3×2×8，因为现在这里有2个anchor box，而  是8维的。 维度是8，因为我们有3个对象类别，如果你有更多对象，那么 的维度会更高。

      - 怎么选择anchor box

        - 手工
          人们一般手工指定anchor box形状，你可以选择5到10个anchor box形状，覆盖到多种不同的形状，可以涵盖你想要检测的对象的各种形状​
        - k-平均算法

    - YOLO algorithm

  - Face recognition

    - face verification
      1 to 1,根据ID卡判断这个人脸是不是

    - face recognition
      1 to K
      ​

    - One-shot learning

      - Similarity函数

        ![img](https://img.mubu.com/document_image/af9d8b8f-1ca4-47fd-b830-ee14b18cf7a6-775812.jpg)

      - 判断差异值是不是小于阕值

    - Siamese network

      ![img](https://img.mubu.com/document_image/21013d6d-b8ec-4a9f-a48f-c7d8fd15ed80-775812.jpg)

      输出为128维

    - Triplet 损失

      用三元组损失的术语来说，你要做的通常是看一个 Anchor 图片，你想让Anchor图片和Positive图片（Positive意味着是同一个人）的距离很接近。然而，当Anchor图片与Negative图片（Negative意味着是非同一个人）对比时，你会想让他们的距离离得更远一点。

      - Learning Object

        ![img](https://img.mubu.com/document_image/5b181947-995a-4937-b9dc-419bb8ce6e70-775812.jpg)

      - Loss Function

        ![img](https://img.mubu.com/document_image/76a36e76-37d2-45ea-9af8-f37cacf47520-775812.jpg)

      - A, P, N

        ![img](https://img.mubu.com/document_image/ce857a06-5106-4325-884b-1fc3b78cbecd-775812.jpg)

    - Face verification and binary classification

      把人脸验证当作一个监督学习，创建一个只有成对图片的训练集，不是三个一组，而是成对的图片，目标标签是1表示一对图片是一个人，目标标签是0表示图片中是不同的人。利用不同的成对图片，使用反向传播算法去训练神经网络，训练Siamese神经网络

      - 不需要每次都计算这个嵌入，你可以提前计算好，那么当一个新员工走近时，你可以使用上方的卷积网络来计算这些编码

  - Neural style transfer

    - What are deep ConvNets learning
      神经风格迁移的关键是理解卷积神经网络每个网络层中学习的视觉表征。前面的层学习简单的特征，如边缘，后面的特征学习复杂的物体，如脸、脚、汽车。
    - Cost function
      - J(G) = alpha * J_content(C,G) + beta * J_style(S,G)
        其中，G 是生成的图像，C 是内容图像，S 是风格图像。该学习算法简单地使用梯度下降最小化与生成图像 G 有关的代价函数。
        步骤：
        \1. 随机生成 G。
        \2. 使用梯度下降最小化 J(G)，也就是 G := G-dG(J(G))。
        \3. 重复步骤 2。

## Sequence Models

- Why Sequence Models
  序列模型在语音识别、音乐生成、情感分类、DNA 序列分析、机器翻译、视频活动识别、命名实体识别等方面得到应用

- Notation

  - 尖括号表示第几个字，圆括号表示第几句话
  - One Hot
  - <UNK>

- RNN

  - Standard network weakpoint

    - Inputs, outputs can be different lengths in different examples
    - Doesn't share feathures learned across different positions of text

  - Simple structure

    ![img](https://img.mubu.com/document_image/ab2b3f8b-9550-4079-b768-d5dad0691f6a-775812.jpg)

  - Activation Function

    - 最常用的是tanh, 也会用relu
    - 二分问题用sigmoid， K分问题用softmax

  - Forward Structure

    ![img](https://img.mubu.com/document_image/c5d246dc-4163-4be5-a605-b47f27383877-775812.jpg)

  - Backpropagation through time

    - Loss function

      ![img](https://img.mubu.com/document_image/795b079e-ae4c-49dd-b11c-3e4f39261397-775812.jpg)

    - 

      ![img](https://img.mubu.com/document_image/76bf0882-2b70-40b3-86a6-91d7683cb16a-775812.jpg)

  - Different types of RNNs

    ![img](https://img.mubu.com/document_image/6315c42f-41d0-4c7e-b82a-1152b237fb61-775812.jpg)

  - Sampling novel sequences

    - Character-level language model
    - Word-level language model
      然语言处理的趋势就是，绝大多数都是使用基于词汇的语言模型

  - Gated Recurrent Unit（GRU）

    - Simple

      ![img](https://img.mubu.com/document_image/bd4080ea-c976-4c09-9212-679630f0d9d4-775812.jpg)

      Update gate

    - Full

      ![img](https://img.mubu.com/document_image/469063ac-dd20-4bf5-9b70-f5a637fafe62-775812.jpg)

      update gate and relevant gate

  - LSTM（long short term memory）unit

    - update gate, forget gate and output gate

      ![img](https://img.mubu.com/document_image/bb50829e-f7f8-47a8-a27a-a1bf1e7772c3-775812.jpg)

  - Bidirectional RNN

    ![img](https://img.mubu.com/document_image/f33787c0-7cba-4d49-af5b-9662bd590353-775812.jpg)

  - Deep RNN

    ![img](https://img.mubu.com/document_image/82f2b795-eeed-443b-a535-83f6f900b931-775812.jpg)

- Natural Language Processing and Word Embeddings

  - Word2Vec

    - CBOW
      根据中心词W(t)周围的词来预测中心词

    - Skip-gram
      根据中心词W(t)来预测周围词​

    - Skip Window
      代表着我们从当前input word的一侧（左边或右边）选取词的数量

    - 抽样率

      - 高频词问题
        “the”这种常用高频单词，训练样本并不会给我们提供有用信息，还会大量训练
      - 抽样模式
        对于我们在训练原始文本中遇到的每一个单词，它们都有一定概率被我们从文本中删掉，而这个被删除的概率与单词的频率有关
      - 
      - 抽样率公式
        计算在词汇表中保留某个词概率的公式，如果这个词出现次数过高，这个值就会很低，就会被抽样，否则就会被完整保留
        ​

    - negative sampling

      它是用来提高训练速度并且改善所得到词向量的质量的一种方法。不同于原本每个训练样本更新所有的权重，负采样每次让一个训练样本仅仅更新一小部分的权重，这样就会降低梯度下降过程中的计算量

      - 只使用1个positive sample和6个negative sample来代替10000个里面算一个，效率高

- Sequence models & Attention mechanism

  - sequence to sequence

    ![img](https://img.mubu.com/document_image/0c880bce-aeed-4a38-b5d9-f1400befcf3f-775812.jpg)

    - Beam Search

      ![img](https://img.mubu.com/document_image/8025b608-77d3-4b5f-9c6e-fb9aac4ab7af-775812.jpg)

      贪婪算法只会挑出最可能的那一个单词，然后继续。而集束搜索则会考虑多个选择，集束搜索算法会有一个参数B，叫做集束宽（beam width）。在这个例子中我把这个集束宽设成3，这样就意味着集束搜索不会只考虑一个可能结果，而是一次会考虑3个。

    - Refinements to Beam Search

      - 乘积通过log变成求和，求平均值（使用个范数d）

        ![img](https://img.mubu.com/document_image/624d6f5f-9537-458d-b15c-578bf5d83637-775812.jpg)

        求平均值的原因是，如果变成求和，那么肯定是加数越多值越大，会让翻译生成更多字，这是不好的，所以加约束Ty（就是字数），再加上范数d

    - Error analysis in beam search

      - 错误分析

        ![img](https://img.mubu.com/document_image/a302776c-ddf7-41bd-a247-65c752d4d207-775812.jpg)

  - Attention Model

    - 注意力模型让一个神经网络只注意到一部分的输入句子。当它在生成句子的时候，更像人类翻译。

      ![img](https://img.mubu.com/document_image/9eb843f3-662a-4a85-9171-74583ea78dd4-775812.jpg)

      ![img](https://img.mubu.com/document_image/4312ab0a-e005-4cfc-a727-b60d806a6c3f-775812.jpg)

      原来的输入X1变成了a1*X1 + a2*X2 + a3*X3 + a4*X4，这样就是一次看4个字而不是一个，其中a1+a2 + ... + an = 1
      要如何算a这个参数呢​，就是把S<t-1> 和 at扔到一个小型神经网络里，生成e，对每一个t生成一个e，对所有e做softmax，就得到权重啦

- Pooling layers

  - max/average pooling

    ![img](https://img.mubu.com/document_image/57288d51-dfc3-4b7f-9881-7ccaf705b56b-775812.jpg)