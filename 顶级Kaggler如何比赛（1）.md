# 顶级Kaggler如何比赛（1）

## 前言

本文是Coursera课程[How to Win a Data Science Competition: Learn from Top Kagglers](https://www.coursera.org/learn/competitive-data-science/home/welcome)的笔记。

这是Advanced Machine Learning系列的第二课。制作方是俄罗斯国立高等经济大学，我的学习感受是这个课程不长，但是知识量很大，教学方式也和我们从小到大差不多，不是欧美那种循序渐进，反而有点“硬灌”的感觉。如果说NG的课程是慢慢推你到山顶，这个就有点攀岩的意思了，不过讲的不错，作业练习特别好，比如第一门课程[Introduction to Deep Learning](https://www.coursera.org/learn/intro-to-deep-learning/home/welcome)已经能涵盖目前深度学习面试的大部分问题了。

本文是第二门课程，讲的是这帮俄国人怎么玩Kaggle的，讲课人都是拿过Kaggle比赛前三的，还有一个H2O.ai的首席科学家。讲的内容都是各种奇技淫巧，和市面上写Kaggle的文章都不一样。话不多说，我们开始吧。

## 比赛机制

### Data

各种各样的数据，CSV，database dump， pictures archive等等

Data的Description用来理解我们要干啥，那些特征能被抽取。

### Model

比赛中建立的模型，注意model不是特定algorithm，而是把数据转化为答案的东西，呵呵

model要符合两个原则：产生最好的预测和可重现。

模型也许会非常复杂，如下

![1](/img/顶级Kaggler如何比赛/1.JPG)

### Submission

通常的sample submission，只有预测。例子如下：

```
1461,169277.0524984
1462,187758.393988768
1463,183583.683569555
... ...
```

### Evaluation

检查模型有多棒 

$`(Predictions, Right Answer) -> Scores`$

***各种方法***：

- Accuracy
- Logistic Loss
- AUC
- RMSE
- MAE

### Leaderboard

评价你的模型和别人比谁比较好的榜单

分成两部分：

**Public Test**: 比赛期间用的，用户能看到数据

**Private Test**：最终成绩用的，用户看不到数据

一般流程如下图：

![2](/img/顶级Kaggler如何比赛/2.JPG)

### 真实应用 vs 比赛

|    方面     | 真实世界 |  比赛  |
| :-------: | :--: | :--: |
|   定义问题    |  Y   |  N   |
| 选择metric  |  Y   |  N   |
|    部署     |  Y   |  N   |
|   推断速度    |  Y   |  N   |
|   数据收集    |  Y   | N/Y  |
|   模型复杂度   |  Y   | N/Y  |
| 目标metric值 |  Y   |  Y   |

**Do not limit yourself**

可以：

- Heuristic
- 手动处理数据

勇于：

- 应用复杂解决方案
- 高级特征工程
- huge计算量

## 主要ML算法概览

### Linear model

概括：找到一个平面，把数据分成两部分

有点：线性模型对稀疏高维数据特别有效

缺点：通常，数据没法被简单模型分隔

例子：

- SVM
- Logistic Regression

SKlearn最流行，Vowpal Wabbit特别适合大数据

### Tree-based

- Decision Tree
- Random Forest
- GDBT

最好的默认方法，建议最开始就用，同时也是冠军算法

就靠分隔数据，无法捕获线性依赖，比如：

![3](/img/顶级Kaggler如何比赛/3.JPG)

使用tree将空间切分能够分隔绿和灰点，那分割线旁边的点很容易出错，线性分隔就会很好

**实现**

- SKlearn实现Random Forest特别好，SKlearn的GDBT不灵
- XGBoost和LightGBM又快又好，XGBoost不用处理*missing value*

[Explanation of Random Forest](http://www.datasciencecentral.com/profiles/blogs/random-forests-explained-intuitively)

[Explanation/Demonstration of Gradient Boosting](http://arogozhnikov.github.io/2016/06/24/gradient_boosting_explained.html)

### KNN

不同distance区别比较大

KNN可以用来做feature engineering，信息量很大

SKlearn的KNN很丰富，很多距离可以用，还能自己实现

[Example of kNN](https://www.analyticsvidhya.com/blog/2014/10/introduction-k-neighbours-algorithm-clustering/)

### Neural Networks

黑盒算法

图片，声音，文字，序列问题有优势

### No Free Lunch Theorem

没有哪个方法在任何任务上都比其他方法更好（*Here is no method which outperforms all others for all tasks*）

或者说

对于一个特别方法，我们总能找到个任务，让这个方法表现的不会最好（*For every method we can construct a task for which this particular method will not be the best*）

dive deep on:

![04](/img/顶级Kaggler如何比赛/4.JPG)

http://scikit-learn.org/stable/auto_examples/classification/plot_classifier_comparison.html

### 结论

- 没有“银弹”算法
- Linear model将空间分成两个子空间
- Tree-based method降空间分成盒子（boxes）
- KNN严重依赖如何计算点点距离
- NN可以产生平滑的非线性边界

## 软硬件要求

### 硬件

大部分比赛： 高配笔记本，16G+ RAM， 4+ cores

高级配置：塔机，32G+ RAM，6+ cores

RAM越大越好，Cores越多越好，存储SSD很重要。

### 云服务

- Amazon AWS（**spot option!**）
- Microsoft Azure
- Google Cloud

### 语言

Python

### 四剑客

- NumPy
- Pandas
- Scikit Learn
- Matplotlib

### IDE

- IPython
- Jupyter

### 特殊工具

- XGBoost
- LightGBM
- Keras
- tsne

### 其他工具

- VOWPAL WABBIT
- libfm
- libffm
- fast_rgf

### 结论

不要迷信软硬件， 呵呵

本章的作业很值得一做， Pandas玩法还挺多的

## 特征预处理和特征生成

### 概览

特征预处理和生成非常有用，但是他们强烈依赖你要使用的模型。

**预处理**：

| pclass |  1   |  2   |  3   |
| :----: | :--: | :--: | :--: |
| target |  1   |  0   |  1   |

上图是Titanic里Pclass和target之间的关系，你会发现；两者关系强烈。但是如果使用线性模型，模型完全无法发现关系。怎么办？把pclass onehot处理就ok了。但是如果用树模型，就没有这个问题。

**生成**：

![5](/img/顶级Kaggler如何比赛/5.JPG)

上图是要预测苹果产量，数据划分是根据时间做的。

![6](/img/顶级Kaggler如何比赛/6.JPG)

增加个passed week属性，突然发现特别管用。线性模型一下就发现关系了。

但是如果要用GBDT找mean target value，那问题来了

![7](/img/顶级Kaggler如何比赛/7.JPG)

GDBT没有见过Week 6的数据，就只能用Week 5的数据去预测，这个属性变成了误导属性。

### 数字特征

**预处理：**

首先，区分ID和数字类型

树模型和非数模型的数据预处理是完全不一样的

![8](/img/顶级Kaggler如何比赛/8.JPG)

**Scaling**

线性模型，KNN和神经网络对scaling很敏感，最好数字特征统统做一遍

- **To [0, 1]**

*sklearn.preprocessing.MinMaxScaler*

$`X = (X-X.min())/(X.max()-X.min())`$

- **To mean=0, std=1**

*sklearn.preprocessing.StandardScaler*

$`X=(X-X.mean())/X.std()`$

**Outliers**

异常点影响判断，最好干掉

先画个图看看啊，比如：

```python
pd.Series(x).hist(bins=30)
```

可以把边缘值切掉，比如

```
UPPERBOUND, LOWERBOUND = np.percentile(x, [1, 99])
y = np.clip(x, UPPERBOUND, LOWERBOUND)
pd.Series(y).hist(bin=30)
```

**Rank**

rank也是个好办法，注意把train，test所有数据连起来一起做

*scipy.stats.rankdata*

```python
rank([-100, 0, 1e5]) = [0,1,2]
rank([1000,1,10]) = [2,0,1]
```

**Log and Sqrt**

这种方法可以将小的变大，大的变小

```python
np.log(1+x)
np.sqrt(x+2/3)
```

针对KNN， Linear模型和神经网路，使用不同的scale方法生成多个模型再整合，可能会有奇效哦

**生成：**

- 先验知识
- 数据探索（EDA）

**例子：**

根据面积和价格生成房子单价

![9](/img/顶级Kaggler如何比赛/9.JPG)

根据坐标生成距离

![10](/img/顶级Kaggler如何比赛/10.JPG)

利用小数部分

![12](/img/顶级Kaggler如何比赛/12.JPG)

这种利用小数的技巧有时候很有用，比方说拍卖出价，如果出的价格有小数，很有可能是假的。或者如果发现每隔一秒访问一次页面，很可能是假用户

**分类和序数特征（Categorical and ordinal features）**

categorical和ordinal的区别在于ordinal的顺序有一定意义，而categorical的顺序没有意义。

***Ordinal***特征例子：

*Ticket class*: 1,2,3

*Driver's license*: A,B,C,D

*Education*:  kindergarden, school, undergraduate, bachelor, master, doctoral

使用**Label encoding**处理ordinal特征，注意顺序，Tree based模型处理这种很好

这两种特征的处理方法：

- 根据字母顺序或者排序顺序处理

  *sklearn.preprocessing.LabelEncoder*

```python
[S,C,Q] -> [2, 1, 3]
```

- 根据出现顺序排序

  *Pandas.factorize*

```python
[S,C,Q] -> [1, 2, 3]
```

- 根据出现频率排序（Frequency encoding）

```python
encoding = titanic.groupby('Embarked').size()
encoding = encoding/len(titanic)
titanic['enc'] = titanic.Embarked.map(encoding)
```

	tree-based和non-tree based模型都能从frequency中受益，因为他记录的特征的分布

	如果很多类别，导致出现频率都一样，那模型就无法分辨了，需要在特征频率上多一些创意，比如组合，或者用*rankdata*

- One-hot encoding

  *pandas.get_dummies*， *sklearn.preprocessing.OneHotEncoder*

  non-tree model处理label encoding不灵，用**one-hot encoding**, one-hot是scale过得，不需要再处理

  如果数据数字特征少，one-hot特别多的话，tree-model很难提高成绩

  one-hot太多，可以用稀疏矩阵存储，省空间，很多算法接受这种输入

**类别特征生成**

类别特征也可以生成，如下图，对线性模型，KNN非常有用

![13](/img/顶级Kaggler如何比赛/13.JPG)

### 时间和坐标特征

**Date和Time**

- Periodicity

  星期几， 月份， 季， 年， 秒， 分， 小时

- 自从（Time since）

  - 独立时间

    比如：从 00:00:00 UTC, 1 January 1970

  - 依赖重要时刻的时间

    到下个节日要多久/上个节日过了多久

  ![14](/img/顶级Kaggler如何比赛/14.JPG)

- 时间差

  - *datetime_feature_1 - datetime_feature_2*

  ![15](/img/顶级Kaggler如何比赛/15.JPG)

**坐标（Coordinates）**

坐标这个很费劲

用预测房价举例子：

首先看看这个坐标附近有没有什么商场学校啥的，可能有用，这种信息可能在训练测试集里，要是没有自己去找，找到后计算distance作为特征

要是没有这种信息，可以把地图分成网格，找到最贵的网格，把其他网格到这的distance作为特征

或者通过聚类把中心点作为重要点，其他点到中心点的distance作为特征

或者找特殊的地区，比方说找到特别老的建筑，算distance作为特征

还以一招是利用聚合统计，比方说计算坐标点附近的人口数，实际地价平均值等等

最后一招就是只折腾坐标，比方说你发现特征关系如下图左，这样通过tree预测就很麻烦，那就旋转坐标如下图右，关系就很清晰了

![16](/img/顶级Kaggler如何比赛/16.JPG)

（我感觉坐标很容易出leakage啊）

### 处理缺失值

**隐藏的NaNs**

有时候Nan其实隐藏在分布里，下图左，可以看出特征值服从0~1正态分布，少量`-1`是多出来的，那Nan很可能就是`-1`。但是下图右这种分布就不能用特殊值来替代NaNs，最好老老实实用平均值

![17](/img/顶级Kaggler如何比赛/17.JPG)

**Fillna方法**

- -999，-1等

  分类问题可以用，线性模型会受伤

- mean，median

  线性模型或神经网络可以用用，tree会很难找到关系

  增加**Isnull**特征会让树和神经网络更健壮，缺点是可能列数会翻倍

  变换如下图：

  ![18](/img/顶级Kaggler如何比赛/18.JPG)

- 重建

  如果行之间的数据是有关系的，那重建数据就很容易，不过这很少见

  大部分情况下，要把希望放在特征生成上

**缺失值填充的问题**

填充缺失值要小心，比如下图

![19](/img/顶级Kaggler如何比赛/19.JPG)

如果按照median填充，则温度是0，模型就跑偏了，这种情况在你没什么办法的时候特别容易发生，要小心

另一个要注意的是使用类别特征平均值编码（高级技巧，后面会讲到）的时候，如果数字特征的NaNs，先用-999填充好了，那编码出来的结果非常糟糕，如下图：

![20](/img/顶级Kaggler如何比赛/20.JPG)

**所以使用特征生成技巧，首先务必忽略NaNs**

**异常点**

有的时候要把异常点当成缺失值，比方说音乐作品的发布时间是2025之后，或者古罗马之前，都应该当做缺失值处理

**连接训练和测试集数据**

有时候测试集的数据可以帮助训练集填充缺失值，如果训练集填充默认值，那模型处理这个特征可能会很随意，测试集的数据会被忽略，这很糟

**frequency**特征可能会明显帮助模型找到内在关系，如下图

![21](/img/顶级Kaggler如何比赛/21.JPG)

通过frequency encoding，发现**C**和**D**都只出现一次，那如果这个特征有用，模型会立刻发现（俄国人这套瞎蒙套路真有用吗？会不会误导小朋友？）。

所以请连接你的训练和测试数据

另外**Xgboost**可以处理NaN

## 从文本和图片中抽取特征

Kaggle比赛分三种

1. 单独的Text/images
2. 普通特征+文本
3. 普通特征+图片/文本

### 词袋（BOW）

**文本->向量**

- **BOW**

  ![22](/img/顶级Kaggler如何比赛/22.JPG)

  *sklearn.feature_extraction.text.CountVectorizer*

- **TFiDF**

  Term frequency:

  ```python
  tf = 1 / x.sum(axis=1) [:,None]
  x = x * tf
  ```

  Inverse Document Frequency:

  ```python
  idf = np.log(x.shape[0] / (x > 0).sum(0))
  x = x * idf
  ```

​	*sklearn.feature_extraction.text.TfidfVectorizer*

- **N-grams**

  *sklearn.feature_extraction.text.CountVectorizer:*
  ***Ngram_range***, ***analyzer***

**预处理**

- Lowercase

- Stemming（词干提取）

  ```
  democracy, democratic, and democratization -> democr
  ```

- Lemmatization（词形还原）

  ```
  democracy, democratic, and democratization -> democracy
  ```

- Stopwords

  - 冠词和介词

    NLTK, Natural Language Toolkit library for python

  - 出现很多次的词

    *sklearn.feature_extraction.text.CountVectorizer:*
    ***max_df***

### Word2vec

词：*Word2vec, Glove, FastText*

句子：*Doc2vec*

使用pretrain model，记得预处理

**BOW和word2vec比较**

**BOW**

- 向量非常长
- 每个值都有意义

**Word2vec**

- 向量相对短
- 一些情况下向量内的值可以解释
- 意思相近的词有相近的向量

### 图片

**图片->向量**

**Pretrain模型**

看图

![23](/img/顶级Kaggler如何比赛/23.JPG)

注意后面层的输出是整体，中间层或前面输出的是细节。比如如果给车分类，那最好取后面层的输出信息，如果是判断药品细节，那就用前面的连接层输出信息或者干脆从头训练

认真选择Pretrain model很重要，vgg， resnet，googlenet等等

**Finetuning**

数据小就finetuning，数据大就拿模型重新train（分布相似也很重要，这是NG说的）

**数据增强**

图片旋转，切割，加噪音等等



